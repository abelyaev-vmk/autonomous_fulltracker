## Autonomous FullTracker
#### Current status: dev mode

This project contains of several autonomous components:

* ImageParser - parse video to images

* FaceDetector - detect faces on video

* FaceTracker - cluster faces on video + face-encoder

* EyeTracker - track eyes on face

* BodyTracker - track person's keypoints

* Speaking Leaps Detector - detect is the person speaking

* Scene Change Detector - detect changing of scene

* Voice Separator - separate audio for two voices

* Status receiver

* Logger

#### Main dependencies
Linux: rabbitmq, python2, python3 

python2: mxnet, sklearn, theano, keras

python3: caffe-faster-rcnn

#### Run FullTracker

This project consists of several blocks, each of which is a unique autonomous unit with its own task.
All blocks are works asynchronously, connection is supported by the program.

To initialize system run `./run_receivers.sh`

To stop all processes run `./kill_receivers.sh`

To reset all rabbitmq settings and queues run `sudo ./reset_rabbitmq.sh`

After initialization, you can asynchronously add new tasks with.

* Run `python2 tracker.py [-v|--video PATH_TO_VIDEO] [-d|--dir PATH_TO_FOLDER_WITH_VIDEO]` 

* Or edit `DATA/VIDEOS_INFO.json` with respect to your dataset and run `python2 process_dataset.py`

After each task is done, notification with time information will be appear in parent-shell

#### Data processing
Data, processed and created by FullTracker, will be stored in `_TMP_DIR`, which will be automatically created in initialization step.

Data separates by video_name. For each video, processed by FullTracker, directory `_TMP_DIR\VIDEO_NAME` will be created, and all temporary files for this video will be stored in it

Temporary files are: 

* parsed images (in `images` directory)

* detected faces (in `VIDEO_NAME_faces.json`)

* Clustered faces (in `VIDEO_NAME_faces_idN.json`)

* Faces embeddings (in `VIDEO_NAME_faces_idN.npy`)

* BodyTracker keypoints and limb maps (in `VIDEO_NAME_body-keypoints.npy`)

* detected and classified eyes (in `VIDEO_NAME_eyes_idN.json`)

* Separated voices (in `VIDEO_NAME_N.wav`)

#### TODO

* Logging (*done*)

* Visualization

* Merge 2 nets in EyeTracker

* Features parser

* Data processing - delete unnecessary files like parsed images, detected faces, etc. (*done*)
