"""
NeurodataLab LLC 21.11.2017
Created by Andrey Belyaev

Face detector, based on caffe
"""

import json
import os
import os.path as osp
import sys
import yaml
from easydict import EasyDict as edict


def add_path(path):
    if path not in sys.path:
        sys.path.insert(0, path)

this_dir = osp.dirname(__file__)

# Add caffe to PYTHONPATH
caffe_path = osp.join(this_dir, 'caffe-faster-rcnn', 'python')
add_path(caffe_path)

# Add lib to PYTHONPATH
lib_path = osp.join(this_dir, 'lib')
add_path(lib_path)

import caffe
from core.config import cfg, cfg_from_file, cfg_from_list
from core.detector_model import DetectorModel
from core.test import test_image_collection
from datasets.collections import ImagesCollection


# Initialize caffe net. Returns function to test_folder
# Usage:
#  test_net = init_model(global_config)
#  json_detection_path = test_net(video_name, data_path)
def init_model(global_config):
    caffe.set_mode_gpu()
    caffe.set_device(global_config.FACEDETECTOR.GPU)
    cfg_from_file(global_config.FACEDETECTOR.CONFIG)
    cfg.MODEL.PRETRAINED_MODEL_CONFIG = global_config.FACEDETECTOR.PROTO

    model = DetectorModel(cfg.MODEL)
    fd, test_prototxt = model.create_temp_test_prototxt()
    net = caffe.Net(test_prototxt, global_config.FACEDETECTOR.NET, caffe.TEST)
    caffe.optimize_memory(net)
    net.name = os.path.splitext(os.path.basename(global_config.FACEDETECTOR.NET))[0]

    os.close(fd)
    return test_net_creator(net, model, global_config)


def test_net_creator(net, model, global_config):
    def test_net(video_name, data_path):
        out_dir = osp.join(global_config.GLOBAL.TMP_DIR, video_name)
        output_path = osp.join(out_dir, video_name + '_faces.json')
        collection_dict = {
            'TYPE': 'IMAGES_DIR',
            'MAX_SIZE': 600,
            'SCALES': [300],
            'PATH': data_path
        }
        image_collection = ImagesCollection(collection_dict)
        extractor = test_image_collection(net, model, image_collection, None)
        total_result = None
        for image_indx, (result, feats) in enumerate(extractor):
            total_result = result
            if image_indx % 200 == 0:
                with open(output_path, 'w') as f:
                    json.dump(total_result, f, indent=2)

        with open(output_path, 'w') as f:
            json.dump(total_result, f, indent=2)

        return output_path

    return test_net


# For debug
if __name__ == '__main__':
    with open('/home/mopkobka/NDL-Projects/FullTracker_v2/config.yml', 'r') as f:
        gc = edict(yaml.load(f))
    init_model(gc)('_Chelsea12.06', '/home/mopkobka/NDL-Projects/FullTracker_v2/_TMP_DIR/_Chelsea12.06/images')