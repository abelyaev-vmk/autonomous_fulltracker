#!/usr/bin/env bash

CONF="${PWD}/config.yml"
CUSTOM_CONF=0
for i in "$@"
do
    case $1 in
        -c=*|--conf=*|--config=*)
        CONF="${i#*=}"
        CUSTOM_CONF=1
        shift
        ;;

        *)
        ;;
    esac
done

if (($CUSTOM_CONF == 0))
then
    python2 tracker_utils/generate_configs.py
fi

python2 receivers/run_recievers.py
python2 receivers/timer_receiver.py --config="$CONF" &
python2 receivers/logs_receiver.py --config="$CONF" &
python2 receivers/image_parser_receiver.py --config="$CONF" &
python2 receivers/body_tracker_receiver.py --config="$CONF" & sleep 5
python3 receivers/face_detector_receiver.py --config="$CONF" & sleep 5
python2 receivers/face_tracker_receiver.py --config="$CONF" & sleep 5
python2 receivers/eye_tracker_receiver.py --config="$CONF" & sleep 5
python2 receivers/scene_change_detector_receiver.py --config="$CONF" & sleep 5
python2 receivers/voice_separator_receiver.py --config="$CONF" & sleep 5
python2 receivers/final_processing.py --config="$CONF" --visualize --copy-voices --copy-images & sleep 3
python2 receivers/status_receiver.py