"""
NeurodataLab LLC 22.01.2018
Created by Andrey Belyaev
"""
import os.path as osp
import sys

sys.path.insert(0, osp.dirname(osp.abspath(__file__)))

import wavreadwrite as wrw
from keras_models import IdentModel, OverlapModel
from utils import *


class VoiceSeparatorModel:
    def __init__(self, conf):
        self.conf = conf
        self.ident_model = IdentModel(conf)
        self.overlap_model = OverlapModel(conf)

    def process_audio(self, audio_path, video_name, out_path, fragmets=((1, 10), ), fps=30):
        X, fs = wrw.read_wave(audio_path)
        assert fs == self.conf.FS
        # x = get_fragments(x, fs, fragmets, fps)

        five_minutes_len = int(5 * 60 * fs)
        xs = [X[list(range(i * five_minutes_len, min(X.shape[0], (i + 1) * five_minutes_len)))]
              for i in range(X.shape[0] // five_minutes_len + 1)]
        if len(xs[-1]) < five_minutes_len * 0.8 and len(xs) > 1:
            xs[-2] = np.concatenate([xs[-2], xs[-1]])
            xs = xs[:-1]

        for x_n, x in enumerate(xs):

            n_smp, num, f_num, max_freq, w_len, hop, sound_len, fs = self.overlap_model.params
            assert fs == self.conf.FS

            step = int(self.conf.t_step * fs)

            mark_a1b1, ph_bad = getmark_a1b1(x, sound_len, step, self.overlap_model)
            ph_a1 = new_phrases(x, mark_a1b1, fs)

            ph_long = np.array([p for p in ph_a1 if p[2] >= self.conf.min_snd_len], dtype=int)
            ph_rest = np.array([p for p in ph_a1 if self.conf.min_snd_len > p[2] > self.conf.rest_len], dtype=int)

            idxs = np.argsort(ph_long[:, 2])[::-1]
            ph_long = ph_long[idxs]

            n_smp, num, f_num, num_flt, max_freq, w_len, hop, sound_len, fs = self.ident_model.params
            assert fs == self.conf.FS

            mel_bank = self.ident_model.get_mel_filter_bank(max_freq, num_flt, w_len, fs)
            attempts = 20
            fr = [Fragment(i, x, ph_long[i], self.conf.big_w_in, self.ident_model,
                           attempts, mel_bank, self.conf.omit, self.conf.mfcc_num)
                  for i in range(ph_long.shape[0])]
            res = compare(fr, attempts, self.ident_model, self.conf.fn)

            if np.all(res <= 0.5):
                group = [list(range(ph_long.shape[0]))]
                base = group
            else:
                group, base, init_group = define_groups(res, fr, ph_long)

            ph, mrk = define_rest(group, base, fr, ph_bad, ph_rest, self.conf.fn, attempts, self.ident_model, x,
                                  self.conf.big_w_in, self.conf.min_snd_len, mel_bank, self.conf.omit, self.conf.mfcc_num)

            y0 = np.zeros((len(x),))
            y1 = np.zeros((len(x),))

            for i in range(len(mrk)):
                if mrk[i] == 0:
                    y0[ph[i, 0]:ph[i, 1]] = x[ph[i, 0]:ph[i, 1]]
                if mrk[i] == 1:
                    y1[ph[i, 0]:ph[i, 1]] = x[ph[i, 0]:ph[i, 1]]

            wrw.write_wave(osp.join(out_path, video_name + '_part%d_0.wav') % x_n, y0, fs)
            wrw.write_wave(osp.join(out_path, video_name + '_part%d_1.wav') % x_n, y1, fs)

            del y0, y1, ph, mrk, group

        wrw.write_wave(osp.join(out_path, video_name + '_full.wav'), X, fs)


if __name__ == '__main__':
    import yaml
    from easydict import EasyDict as edict
    with open('/home/mopkobka/NDL-Projects/autonomous_fulltracker/config.yml', 'r') as f:
        C = edict(yaml.load(f))

    vsm = VoiceSeparatorModel(C.VOICESEPARATOR)


