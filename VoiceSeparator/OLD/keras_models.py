"""
NeurodataLab LLC 19.01.2018
Created by Andrey Belyaev
"""
import numpy as np
import keras.backend as K
from keras.layers import Concatenate, Conv1D, Dense, GlobalAveragePooling1D, Input, Lambda, MaxPooling1D
from keras.models import load_model, Model


class IdentModel:
    def __init__(self, conf):
        print('Loading ident model')
        self._params = self.read_params(conf.ident_params)

        self._model = load_model(conf.ident_model, compile=False)
        # self.model.compile()

    def predict(self, *args, **kwargs):
        return self._model.predict(*args, **kwargs)

    @staticmethod
    def read_params(path):
        return list(map(int, open(path, 'r').readline().strip().split('\t')))

    @staticmethod
    def get_mel_filter_bank(maxfreq, numflt, wlen, fs):
        min_mf = 0
        max_mf = 1125 * np.log(1 + maxfreq / 700)
        mf = np.linspace(min_mf, max_mf, numflt + 2)
        f = 700 * (np.exp(mf / 1125) - 1)
        f[0] = 0
        f_bin = np.int64(np.floor(f / fs * wlen) + 1)
        flt_bank = np.zeros((numflt, wlen))
        for i in np.arange(numflt):
            for k in np.arange(f_bin[i] + 1, f_bin[i + 1]):
                flt_bank[i, k] = (k - f_bin[i]) / (f_bin[i + 1] - f_bin[i])
            flt_bank[i, f_bin[i + 1]] = 1
            for k in np.arange(f_bin[i + 1] + 1, f_bin[i + 2]):
                flt_bank[i, k] = (f_bin[i + 2] - k) / (f_bin[i + 2] - f_bin[i + 1])
        return flt_bank

    @staticmethod
    def get_mfcc(x, mel_bank):
        from scipy.fftpack import fft, dct
        sp = np.clip(np.abs(fft(x)), 1E-12, None)
        log_mel = np.log(np.sum(sp * mel_bank, axis=-1))
        return dct(log_mel)

    @staticmethod
    def get_x(x, mel_bank, num_flt, w_len, hop):
        num = (len(x) - w_len) // hop + 1
        mfcc = np.zeros((num, num_flt), dtype=np.float32)
        for i in range(num):
            mfcc[i] = IdentModel.get_mfcc(x[i * hop:i * hop + w_len], mel_bank)
        return mfcc

    @staticmethod
    def transform_data(x, num, omit, mfcc_num):
        x = x[:, omit:mfcc_num]
        fn = mfcc_num - omit
        x_min = np.min(x, axis=-1, keepdims=True)
        x_max = np.max(x, axis=-1, keepdims=True)
        x = 2 * (x - x_min) / (x_max - x_min) - 1
        d = np.std(x, axis=0)
        hop = x.shape[0] // num
        z = np.zeros((1, num, fn, 2), dtype=np.float32)
        srt = np.random.permutation(x.shape[0])
        x = x[srt]
        for i in range(num):
            z[0, i, :, 0] = np.mean(x[i * hop:(i + 1) * hop], axis=0)
            z[0, i, :, 1] = d

        return z

    @property
    def params(self):
        return np.array(self._params)

    @property
    def model(self):
        return self._model


class OverlapModel:
    def __init__(self, conf):
        print('Loading overlap model')
        self._params = self.read_params(conf.overlap_params)
        self._model = self.init_model(conf.grad_model, conf.peak_model, conf.sp_model)
        self._model.load_weights(conf.overlap_weights)

    def predict(self, *args, **kwargs):
        return self._model.predict(*args, **kwargs)

    def init_model(self, grad_model, peak_model, sp_model):
        num, f_num = self.params[1:3]
        model_grad = load_model(grad_model)
        model_grad.name = 'model_grad'

        model_peak = load_model(peak_model)
        model_peak.name = 'model_peak'

        model_sp = load_model(sp_model)
        model_sp.name = 'model_sp'

        inp0 = Input(shape=(num,))
        w = Lambda(lambda z: K.expand_dims(z, axis=-1), output_shape=(num, 1))(inp0)
        w = Conv1D(8, 3, activation='tanh')(w)
        w = MaxPooling1D()(w)
        w = Conv1D(16, 3, activation='tanh')(w)
        w = MaxPooling1D()(w)
        w = Conv1D(32, 3, activation='tanh')(w)
        w = MaxPooling1D()(w)
        w = GlobalAveragePooling1D()(w)

        inp1 = Input(shape=(num, f_num))
        x_ = model_sp(inp1)
        inp2 = Input(shape=(num, f_num))
        y_ = model_peak(inp2)
        inp3 = Input(shape=(num, f_num))
        z_ = model_grad(inp3)

        v = Concatenate()([w, x_, y_, z_])
        v = Dense(40, activation='tanh')(v)
        v = Dense(200, activation='tanh')(v)
        v = Dense(5, activation='tanh')(v)
        out = Dense(1, activation='sigmoid')(v)

        return Model(inputs=[inp0, inp1, inp2, inp3], outputs=out)

    @staticmethod
    def read_params(path):
        return list(map(int, open(path, 'r').readline().strip().split('\t')))

    @staticmethod
    def get_x(x, num, f_num, w_len, hop):
        num1 = (len(x) - w_len) // hop + 1
        pwr = np.zeros((num1,))
        sp = np.zeros((num1, f_num), dtype=np.float32)
        sp1 = np.zeros((num1, f_num), dtype=np.float32)
        peak = np.zeros((num1, f_num), dtype=np.float32)
        grad = np.zeros((num1, f_num), dtype=np.float32)
        for i in range(num1):
            pwr[i] = np.mean(np.abs(x[i * hop:i * hop + 1]))
            sp[i] = np.abs(np.fft.fft(x[i * hop:i * hop + w_len]))[:f_num]
            sp1[i] = np.copy(sp[i])
            l = np.concatenate([[0], sp[i]])
            r = np.concatenate([sp[i], [0]])
            max_ind = np.where((l[1:] > l[:-1]) & (r[:-1] > r[1:]) & (l[1:] > 1E-10))[0]
            peak[i, max_ind] = sp[i, max_ind]
            sp[i] = np.clip(sp[i], 1E-12, None)
            sp[i] = np.log(sp[i])
        sp_max = np.abs(np.max(sp1, axis=0, keepdims=True))
        for i in range(num1 - 1):
            grad[i + 1] = (sp1[i + 1] - sp1[i])
        grad /= sp_max
        sp = sp[1:, :]
        grad = grad[1:, :]
        peak = peak[1:, :]
        pwr = pwr[1:]
        assert num == num1 - 1
        for j in range(f_num):
            med = np.mean(sp[:, j])
            skv = np.std(sp[:, j])
            if skv > 0:
                sp[:, j] = (sp[:, j] - med) / skv
            else:
                sp[:, j] = 0
            ind = np.where(peak[:, j] != 0)[0]
            if len(ind):
                p_max = np.max(peak[ind, j])
                p_min = np.min(peak[ind, j])
                if p_max > p_min:
                    peak[ind, j] = (peak[ind, j] - p_min) / (p_max - p_min)
                else:
                    peak[ind, j] = peak[ind, j] / p_max
        pwr = pwr / np.max(pwr)
        return pwr, sp, peak, grad

    @property
    def params(self):
        return np.array(self._params)

    @property
    def model(self):
        return self._model
