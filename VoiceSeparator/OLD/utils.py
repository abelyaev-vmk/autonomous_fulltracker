"""
NeurodataLab LLC 19.01.2018
Created by Andrey Belyaev
"""
import numpy as np
from copy import deepcopy


# Classes

class Phrase:
    def __init__(self, x, fs, twin=0.01, tbigwin=0.2):
        self.x = x
        self.fs = fs
        self.win = int(twin * fs)
        self.big_w_in = int(tbigwin * fs)
        self.big_len = self.big_w_in // self.win

        # power init
        self.num = len(self.x) // self.win
        self.x = self.x[:self.num * self.win]
        self.x_len = len(self.x)
        self.pw = np.zeros((self.num,))
        y = abs(self.x)

        for i in range(self.num):
            self.pw[i] = np.mean(y[i * self.win:(i + 1) * self.win])

        self.pw = np.clip(self.pw, 1E-5, None)
        tmp = np.concatenate(
            [self.pw[0] * np.ones((self.big_len - 1,)), self.pw, self.pw[-1] * np.ones((self.big_len - 1,))])
        self.tmp = 20 * np.log10(tmp)

        big_tmp = np.zeros((self.num + self.big_len - 1,))
        for i in range(self.num + self.big_len - 1):
            big_tmp[i] = np.mean(tmp[i:i + self.big_len])
        big_tmp = 20 * np.log10(big_tmp)

        self.big_pw_pstart = big_tmp[:self.num]
        self.big_pw_pend = big_tmp[self.big_len - 1:]
        self.pw = 20 * np.log10(self.pw)

        i_min = np.argmin(self.big_pw_pend[:-self.big_len + 1])
        self.silence = self.x[i_min * self.win:(i_min + self.big_len) * self.win]
        self.ind_silence = (i_min * self.win, (i_min + self.big_len) * self.win)
        med = 20 * np.log10(np.median(y))
        self.tr_sound = med
        self.tr_phrase = med
        #
        p_end = self.phrase_init(self.pw, self.big_pw_pend)
        p_start = self.num - 1 - self.phrase_init(self.pw[::-1], self.big_pw_pstart[::-1])[::-1]
        self.p_series = self.build_series(p_start, p_end)

    def phrase_init(self, pw, bigpw):

        def _next_sound(pos_):
            while pos_ < self.num and pw[pos_] < self.tr_sound:
                pos_ += 1
            return pos_

        def _phrase_silence(pos_):
            while pos_ < self.num and bigpw[pos_] >= self.tr_phrase:
                pos_ += 1
            return pos_

        def _spec_min(pos_):
            return pos_ + np.argmin(pw[pos_:min(self.num, pos_ + self.big_len)])

        p = []
        pos = 0
        while pos < self.num:
            pos = _next_sound(pos)
            if pos < self.num:
                pos = _phrase_silence(pos)
                if pos < self.num:
                    pos = _spec_min(pos)
                    p.append(pos)
                    while pos < self.num and bigpw[pos] < self.tr_phrase:
                        pos += 1
                else:
                    p.append(self.num - 1)
            else:
                break
        return np.array(p)

    def build_series(self, l_start, l_end):
        series = []
        ind0 = 0
        ind1 = 0
        while ind0 < len(l_start) and ind1 < len(l_end):
            while ind1 < len(l_end) and l_end[ind1] <= l_start[ind0]:
                ind1 += 1
            if ind1 < len(l_end):
                series.append((l_start[ind0], l_end[ind1], l_end[ind1] - l_start[ind0]))
            else:
                break
            while ind0 < len(l_start) and l_start[ind0] < l_end[ind1]:
                ind0 += 1
        return np.array(series, dtype=np.int) * self.win


class Fragment:
    def __init__(self, name, x, p, big_w_in, ident_model, attempts, mel_bank, omit, mfcc_num):
        self.id = name
        self.p = p
        self.x = x[p[0]:p[1]]
        self.n = self.p[2] // big_w_in
        if self.n < 3:
            self.n = 3
        num_flt, w_len, hop = ident_model.params[[3, 5, 6]]
        self.mfcc = ident_model.get_x(self.x, mel_bank, num_flt, w_len, hop)
        X = []
        for i in range(attempts):
            X.append(ident_model.transform_data(self.mfcc, self.n, omit, mfcc_num))
        self.X = np.concatenate(X, axis=0)


# Functions

def get_fragments(x, fs, fragments, fps):
    secs = np.array(fragments) / fps
    idxs = []
    for sec in secs:
        idxs += list(range(int(sec * fs), int(sec * fs + fs / fps)))
    idxs = np.array(list(set(idxs)))
    return x[idxs]


def transform(y, sound_len, step):
    for i in range(1, len(y) - 1):
        if y[i] & ~y[i - 1] & ~y[i + 1]:
            y[i] = 0
    for i in range(1, len(y) - 1):
        if ~y[i] & y[i - 1] & y[i + 1]:
            y[i] = 1
    for i in range(2, len(y) - 3):
        if y[i] & y[i + 1] & ~y[i - 1] & ~y[i - 2] & ~y[i + 2] & ~y[i + 3]:
            y[i] = 0
            y[i + 1] = 0
    for i in range(2, len(y) - 3):
        if ~y[i] & ~y[i + 1] & y[i - 1] & y[i - 2] & y[i + 2] & y[i + 3]:
            y[i] = 1
            y[i + 1] = 1
    st = np.where((y - np.concatenate([[1], y[:-1]])) == -1)[0]
    fin = np.where((y - np.concatenate([y[1:], [1]])) == -1)[0]
    assert len(st) == len(fin)
    area = (3 * sound_len) // (5 * step)
    for i in range(len(st)):
        if fin[i] - st[i] <= 2 * area:
            y[st[i]:fin[i] + 1] = 1
        else:
            y[st[i]:st[i] + area] = 1
            y[fin[i] + 1 - area:fin[i] + 1] = 1
    return y


def getmark_a1b1(x, sound_len, step, overlap_model):
    n_step = (len(x) - sound_len) // step + 1
    x0, x1, x2, x3 = [], [], [], []
    num, f_num, w_len, hop = overlap_model.params[[1, 2, 4, 5]]
    for i in range(n_step):
        pwr, sp, peak, grad = overlap_model.get_x(x[i * step:i * step + sound_len], num, f_num, w_len, hop)
        x0.append(pwr)
        x1.append(sp)
        x2.append(peak)
        x3.append(grad)
    x0, x1, x2, x3 = map(np.array, (x0, x1, x2, x3))
    y = overlap_model.predict([x0, x1, x2, x3], batch_size=1024)
    y = (y > 0.5)[:, 0]
    y = transform(y, sound_len, step)
    mark = np.zeros((len(x),), dtype=np.byte)
    for i in range(n_step - 1):
        if y[i] == 0:
            mark[i * step:(i + 1) * step] = 1
    if y[n_step - 1] == 0:
        mark[(n_step - 1) * step:(n_step - 1) * step + sound_len] = 1
    t = np.concatenate([[0], 1 - mark, [0]])
    st = np.where(t[1:-1] - t[:-2] == 1)[0]
    fin = np.where(t[1:-1] - t[2:] == 1)[0]
    assert len(st) == len(fin)
    ph_bad = []
    for i in range(len(st)):
        ph_bad.append([st[i], fin[i] + 1, fin[i] + 1 - st[i]])
    return mark, np.array(ph_bad, dtype=np.int)


def new_phrases(x, mb1, FS):

    def get_bounds(z, start):
        t = np.concatenate([[0], z, [0]])
        st = np.where(t[1:-1] - t[:-2] == 1)[0]
        fin = np.where(t[1:-1] - t[2:] == 1)[0]
        assert len(st) == len(fin)
        bnd = []
        for j in range(len(st)):
            bnd.append(np.array([start + st[j], start + fin[j] + 1, fin[j] + 1 - st[j]]))
        return bnd

    ph = Phrase(x, FS)
    for i in range(ph.p_series.shape[0] - 1):
        ph.p_series[i, 1] = ph.p_series[i + 1, 0] = (ph.p_series[i, 1] + ph.p_series[i + 1, 0]) // 2
    ph_a1 = []
    for i in range(ph.p_series.shape[0]):
        ph_a1 += get_bounds(mb1[ph.p_series[i, 0]:ph.p_series[i, 1]], ph.p_series[i, 0])
    return ph_a1


def compare(fr, attempts, model, fn):
    res = np.zeros((len(fr), len(fr)))
    for i in range(len(fr) - 1):
        for j in range(i + 1, len(fr)):
            X = np.zeros((attempts, fr[i].n * fr[j].n, fn, 4))
            r = np.zeros((attempts,))
            for k in range(attempts):
                for m in range(fr[i].n):
                    for l in range(fr[j].n):
                        X[k, m * fr[j].n + l, :, :2] = fr[i].X[k, m, :, :]
                        X[k, m * fr[j].n + l, :, 2:] = fr[j].X[k, l, :, :]
            r = np.mean(model.predict(X, batch_size=attempts)[:, 0])
            res[i, j] = np.mean(r)
            res[j, i] = res[i, j]
    return res


def get_neighbours_anti_neighbours(assoc):
    nei = []
    a_nei = []
    for i in range(assoc.shape[0]):
        nei.append([k for k in range(assoc.shape[0]) if assoc[i, k]])
        a_nei.append([k for k in range(assoc.shape[0]) if not assoc[i, k] and k != i])
    return nei, a_nei


def all_agreed_triples(nei, fr):
    r_nei = []
    for i in range(len(nei)):
        r_nei.append([k for k in nei[i] if k > i])
    tr = []
    tr_len = []
    ind1 = np.zeros((len(nei),), dtype=np.int)
    ind2 = np.zeros((len(nei), len(nei)), dtype=np.int)
    ind3 = -1 * np.ones((len(nei), len(nei), len(nei)), dtype=np.int)
    n_ind = 0
    for i in range(len(nei) - 2):
        for j in r_nei[i]:
            for k in sorted(list(set(r_nei[i]) & set(r_nei[j]))):
                tr.append([i, j, k])
                tr_len.append(np.mean([fr[i].p[2], fr[j].p[2], fr[k].p[2]]))
                ind3[i, j, k] = n_ind
                ind2[i, j] = 1
                ind1[i] = 1
                n_ind += 1
    return np.array(tr), np.array(tr_len), np.array(ind1), np.array(ind2), np.array(ind3)


def all_agreed_pairs(nei, fr):
    r_nei = []
    for i in range(len(nei)):
        r_nei.append([k for k in nei[i] if k > i])
    pr = []
    pr_len = []
    n_ind = 0
    ind = -1 * np.ones((len(nei),), dtype=np.int)
    for i in range(len(nei) - 1):
        flag = True
        for j in r_nei[i]:
            pr.append([i, j])
            pr_len.append(np.mean([fr[i].p[2], fr[j].p[2]]))
            if flag:
                ind[i] = n_ind
                flag = False
            n_ind += 1
    return pr, np.array(pr_len), ind


def get_base_triples(tr, tr_len, ind1, ind2, ind3, nei, a_nei):
    ra_nei = []
    groups = []
    gr_lens = []
    n = 0
    for i in range(len(nei)):
        ra_nei.append([k for k in a_nei[i] if k > i])
    for m in range(tr.shape[0] - 1):
        s = sorted(list(set(ra_nei[tr[m, 0]]) & set(a_nei[tr[m, 1]]) & set(a_nei[tr[m, 2]])))
        for i in range(len(s) - 2):
            if ind1[s[i]]:
                for j in range(i + 1, len(s) - 1):
                    if ind2[s[i], s[j]]:
                        for k in range(j + 1, len(s)):
                            if ind3[s[i], s[j], s[k]] >= 0:
                                groups.append([list(tr[m]), [s[i], s[j], s[k]]])
                                gr_lens.append(tr_len[m] + tr_len[ind3[s[i], s[j], s[k]]])
                                n += 1
                                break
    gr_lens = np.array(gr_lens)
    try:
        i_best = np.argmax(gr_lens)
        return groups[i_best]
    except:
        raise NotImplementedError('No base triplets found')


def agreed_extension(gr, lst, assoc):
    def _cmp(s_):
        c0 = [assoc[i, j] for i in gr[0] for j in s_]
        c1 = [assoc[i, j] for i in gr[1] for j in s_]
        if np.all(c0) and not np.any(c1):
            return 0
        elif np.all(c1) and not np.any(c0):
            return 1
        else:
            return 2

    base = set(gr[0] + gr[1])
    flag = True
    while lst and flag:
        lst = [s for s in lst if base & set(s) == set()]
        for s in lst:
            r = _cmp(s)
            if r != 2:
                base &= set(s)
                if r == 0:
                    gr[0] += s
                else:
                    gr[1] += s
                break
        else:
            flag = False
    return gr


def soft_extension(gr, main_lst, res):
    def _cmp(g_, l_):
        return np.mean([np.mean([res[i_, j_] for i_ in g_ for j_ in s_]) for s_ in l_])

    base = set(gr[0] + gr[1])
    lst = [s for s in main_lst if base & set(s) == set()]
    gr0 = []
    gr1 = []
    rest = list(set([t for s in lst for t in s]))
    for t in rest:
        l = [s for s in lst if t in s]
        r0 = _cmp(gr[0], l)
        r1 = _cmp(gr[1], l)
        if r0 < r1:
            gr0.append(t)
        else:
            gr1.append(t)
    gr[0] += gr0
    gr[1] += gr1
    return gr


def define_groups(res, fr, ph_long):
    assoc = (res <= 0.5) * 1
    for i in range(assoc.shape[0]):
        assoc[i, i] = 0
    nei, a_nei = get_neighbours_anti_neighbours(assoc)
    triples, tr_len, tr_ind1, tr_ind2, tr_ind3 = all_agreed_triples(nei, fr)
    pairs, pr_len, pr_ind = all_agreed_pairs(nei, fr)
    singles = [[i] for i in range(ph_long.shape[0])]
    snlen = [ph_long[i, 2] for i in range(ph_long.shape[0])]
    # print('basegroups...')
    group = get_base_triples(triples, tr_len, tr_ind1, tr_ind2, tr_ind3, nei, a_nei)
    triples = [list(s) for s in list(triples)]
    pairs = [list(s) for s in list(pairs)]
    init_group = deepcopy(group)
    # print(group)
    lst = deepcopy(triples)
    # print('agreed 3...')
    group = agreed_extension(group, lst, assoc)
    # print(group)
    lst = deepcopy(pairs)
    # print('agreed 2...')
    group = agreed_extension(group, lst, assoc)
    # print(group)
    base = deepcopy(group)
    # print('soft')
    group = soft_extension(group, triples, res)
    # print(group)
    group = soft_extension(group, pairs, res)
    # print(group)
    group = soft_extension(group, singles, res)
    # print(group)
    return group, base, init_group


def define_rest(group, base, fr, ph_bad, ph_rest, fn, attempts,
                model, x, big_w_in, min_snd_len, mel_bank, omit, mfcc_num):

    def _cmp(f0, f1):
        X = np.zeros((attempts, f0.n * f1.n, fn, 4))
        r = np.zeros((attempts,))
        for k in range(attempts):
            for m in range(f0.n):
                for l in range(f1.n):
                    X[k, m * f1.n + l, :, :2] = f0.X[k, m, :, :]
                    X[k, m * f1.n + l, :, 2:] = f1.X[k, l, :, :]
        r = np.mean(model.predict(X, batch_size=attempts)[:, 0])
        return np.mean(r)

    def _cmp_list(ls):
        res = []
        for l in ls:
            r0 = []
            r1 = []
            for b in base0:
                r0.append(_cmp(l, b))
            for b in base1:
                r1.append(_cmp(l, b))
            if np.mean(r0) <= np.mean(r1):
                res.append(0)
            else:
                res.append(1)
        return res

    def _def_one(k_):
        rest_fr = [Fragment(k_, x, ph[k_], big_w_in, model, attempts, mel_bank, omit, mfcc_num)]
        res = _cmp_list(rest_fr)[0]
        if res == mrk[k_ - 1] == mrk[k_ + 1] == 0:
            return 0
        if res == mrk[k_ - 1] == mrk[k_ + 1] == 1:
            return 1
        return 2

    def _def_two(k_):
        if mrk[k_ - 1] == mrk[k_ + 2] == 2:
            return [2, 2]
        if mrk[k_ - 1] == 2:
            rest_fr = [Fragment(k_ + 1, x, ph[k_ + 1], big_w_in, model, attempts, mel_bank, omit, mfcc_num)]
            res = _cmp_list(rest_fr)[0]
            if res == mrk[k_ + 2]:
                return [2, res]
            else:
                return [2, 2]
        if mrk[k_ + 2] == 2:
            rest_fr = [Fragment(k_, x, ph[k_], big_w_in, model, attempts, mel_bank, omit, mfcc_num)]
            res = _cmp_list(rest_fr)[0]
            if res == mrk[k_ - 1]:
                return [res, 2]
            else:
                return [2, 2]
        rest_fr = [Fragment(k_, x, ph[k_], big_w_in, model, attempts, mel_bank, omit, mfcc_num),
                   Fragment(k_ + 1, x, ph[k_ + 1], big_w_in, model, attempts, mel_bank, omit, mfcc_num)]
        res = _cmp_list(rest_fr)
        if res == [mrk[k_ - 1], mrk[k_ + 2]]:
            return res
        elif res[0] == res[1]:
            if res[0] == mrk[k_ - 1] or res[1] == mrk[k_ + 2]:
                return res
            else:
                return [2, 2]
        else:
            return [2, 2]

    def _def_more(lst_):
        rest_fr = [Fragment(k, x, ph[k], big_w_in, model, attempts, mel_bank, omit, mfcc_num) for k in lst_]
        res = _cmp_list(rest_fr)
        if np.all(np.array(res) == 0) and np.sum(ph[lst_, 2]) > min_snd_len:
            return len(res) * [0]
        if np.all(np.array(res) == 1) and np.sum(ph[lst_, 2]) > min_snd_len:
            return len(res) * [1]
        k = 0
        while k < len(res) - 1 and res[k] == mrk[lst_[0] - 1]:
            k += 1
        l = 0
        while l < len(res) - 1 and res[-1 - l] == mrk[lst_[-1] + 1]:
            l += 1
        if k + l > len(res):
            return len(res) * [res[0]]
        return k * [mrk[lst_[0] - 1]] + (len(res) - k - l) * [2] + l * [mrk[lst_[-1] + 1]]

    p0 = np.array([fr[k].p for k in group[0]])
    m0 = len(group[0]) * [0]  # group 0
    p1 = np.array([fr[k].p for k in group[1]])
    m1 = len(group[1]) * [1]  # group 1
    m2 = ph_bad.shape[0] * [2]  # bad group
    mr = ph_rest.shape[0] * [-1]  # to define
    ph = np.concatenate([p0, p1, ph_bad, ph_rest], axis=0)
    mrk = np.array(m0 + m1 + m2 + mr, dtype=np.int)
    srt = np.argsort(ph[:, 0])
    ph = ph[srt]
    mrk = mrk[srt]
    t = np.concatenate([[0], (mrk == -1) * 1, [0]])
    st = np.where(t[1:-1] - t[:-2] == 1)[0]
    fin = np.where(t[1:-1] - t[2:] == 1)[0]
    assert len(st) == len(fin)
    lst = [np.array([st[i], fin[i] + 1, fin[i] + 1 - st[i]]) for i in range(len(st))]
    if lst and lst[0][0] == 0:
        mrk[lst[0][0]:lst[0][1]] = 2
        del lst[0]
    if lst and lst[-1][1] == len(mrk):
        mrk[lst[-1][0]:lst[-1][1]] = 2
        del lst[-1]
    if lst:
        base0 = [fr[i] for i in base[0]]
        base1 = [fr[i] for i in base[1]]
        for p in lst:
            if p[2] == 1:
                mrk[p[0]] = _def_one(p[0])
            elif p[2] == 2:
                mrk[p[0]:p[1]] = _def_two(p[0])
            else:
                mrk[p[0]:p[1]] = _def_more([k for k in range(p[0], p[1])])

    return ph, mrk
