"""
NeurodataLab LLC 20.02.2018
Created by Andrey Belyaev
"""

import os
import os.path as osp
import mxnet as mx
import numpy as np
from skimage.exposure import adjust_sigmoid, equalize_hist
import hmmlearn.hmm as hmm
from scipy.io import wavfile as wv
from sklearn.decomposition import PCA as pca


class MXVoiceModel:
    def __init__(self, net_path, net_epoch, magic_param, magic_param2, target_size=(40, 80), batch_size=16,
                 ctx=mx.gpu(0)):
        self.sym, self.arg_params, self.aux_params = mx.model.load_checkpoint(net_path, net_epoch)
        self.feat = self.sym.get_internals()[magic_param]
        self.batch_size = batch_size
        self.data_shape = (batch_size, 3, target_size[0], target_size[1])
        self.model = mx.mod.Module(self.feat, data_names=['data'], label_names=magic_param2, context=ctx)
        self.model.bind(data_shapes=[('data', self.data_shape)], for_training=False)
        self.model.init_params(arg_params=self.arg_params, aux_params=self.aux_params,
                               allow_missing=True, allow_extra=True)

        # zero forward
        self.model.forward(mx.io.DataBatch(data=[mx.nd.zeros(self.data_shape)]))

    @staticmethod
    def preprocess(data):
        data_log = np.log(data + 1e-8)

        data_log_norm = (data_log - data_log.min()) / (data_log.max() - data_log.min())
        data_hist = equalize_hist(data_log_norm)
        data_sigmoid = adjust_sigmoid(data_log_norm)

        return np.concatenate([data_log_norm[np.newaxis], data_hist[np.newaxis], data_sigmoid[np.newaxis]])

    def process_data(self, data):
        feats, batch = [], []
        for d in data:
            batch.append(self.preprocess(d))
            if len(batch) < self.batch_size:
                continue
            self.model.forward(mx.io.DataBatch(data=[mx.nd.array(np.array(batch))]))
            outputs = self.model.get_outputs()[0]
            feats.extend(outputs.asnumpy().tolist())
            batch = []

        if len(batch) > 0:
            end_pos = len(batch)
            for _ in range(len(batch), self.batch_size):
                batch.append(np.zeros_like(batch[0]))
            self.model.forward(mx.io.DataBatch(data=[mx.nd.array(np.array(batch))]))
            outputs = self.model.get_outputs()[0]
            feats.extend(outputs.asnumpy()[:end_pos])

        return np.array(feats)

    @staticmethod
    def get_data_from_folder(folder, target_size=(40, 80)):
        ret = {}
        for path in os.listdir(folder):
            d = np.load(osp.join(folder, path))
            if d.shape == target_size:
                ret[path] = d
        return ret


class VoiceSeparator:
    def __init__(self, path_to_clear, path_to_embed, target_size=(40, 80), batch_size=128, max_freq=4000, overlap=50,
                 frame_length=20, length=400, n_components=10, gpu=0, *args, **kwargs):
        self.path_to_embed = path_to_embed
        self.path_to_clear = path_to_clear
        self.target_size = target_size
        self.clear_model = MXVoiceModel(path_to_clear, 0, 'voice_clearing_output', ['voice_clearing_label'],
                                        batch_size=batch_size, ctx=mx.gpu(gpu))
        self.embed_model = MXVoiceModel(path_to_embed, 0, 'feat_biometry_output', None,
                                        batch_size=batch_size, ctx=mx.gpu(gpu))
        self.batch_size = batch_size
        self.max_freq = max_freq
        self.overlap = overlap
        self.frame_length = frame_length
        self.length = length
        self.n_components = n_components
        self.step = length / 1000. * overlap / 100.
        self.xw = int(length / (overlap / 100.) / frame_length)
        self.xh = 80  # init value

    @staticmethod
    def read_wav(path):
        try:
            rate, wav = wv.read(path)
        except:
            import soundfile as sf
            wav, rate = sf.read(path)
        if np.max(np.fabs(wav)) > 1:
            wav = (wav / 32768.).astype(float)
        if len(wav.shape) == 2:
            wav = 0.5 * (wav[:, 0] + wav[:, 1])
        return wav, rate

    @staticmethod
    def write_wav(path, wav, rate):
        if np.fabs(np.max(wav)) <= 1.:
            wv.write(path, rate, (32768 * wav).astype(np.int16))
        else:
            wv.write(path, rate, wav.astype(np.int16))

    @staticmethod
    def estimate_features(wav, rate, maxfreq=4000, overlap=50, framelength=20):
        framelength_s = framelength / 1000.
        framelength_n = int(np.round(framelength_s * rate))
        freqs = np.fft.rfftfreq(framelength_n, d=1. / rate)
        overlap = overlap / 100.
        step = framelength_s * overlap
        length = len(wav) / float(rate)

        t0 = 0
        t1 = t0 + framelength_s
        fft = []
        while t1 < length:
            i0 = int(np.round(t0 * rate))
            i1 = i0 + framelength_n
            x = np.copy(wav[i0:i1])
            x *= np.hamming(len(x))
            f = np.abs(np.fft.rfft(x))
            f = f[freqs < maxfreq]
            fft.append(f)
            t0 += step
            t1 += step
        return np.array(fft)

    def get_sample(self, wav, rate):
        fft = self.estimate_features(wav, rate, self.max_freq, self.overlap, self.frame_length)
        xh = fft.shape[1]
        x = []
        t = []
        for j in range(len(fft) // self.xw * 2 - 1):
            i0 = j * self.xw // 2
            i1 = j * self.xw // 2 + self.xw
            a = fft[i0:i1, :xh]
            if np.max(a) - np.min(a) > 1.e-12:
                x.append(a)
            else:
                x.append(np.zeros(a.shape) + 1.e-12 * np.random.random(size=a.shape))
            t.append(j * self.step)
        return np.array(x, dtype=float), np.array(t, dtype=float)

    @staticmethod
    def get_mask(probabilities):
        l_init = np.argmax(probabilities, axis=1)

        l_smooth1 = np.zeros(len(l_init))
        # remove short non-speech intervals
        for i in range(1, len(l_init) - 1):
            if l_init[i - 1] == 2 and l_init[i] != 2 and l_init[i + 1] == 2:
                l_smooth1[i] = 2
            else:
                l_smooth1[i] = l_init[i]

        l_smooth2 = np.zeros(len(l_smooth1))
        # remove short speech intervals
        for i in range(1, len(l_smooth1) - 1):
            if l_smooth1[i - 1] != 2 and l_smooth1[i] == 2 and l_smooth1[i + 1] != 2:
                l_smooth2[i] = 1
            else:
                l_smooth2[i] = l_smooth1[i]

        l_smooth3 = np.zeros(len(l_smooth2))
        # replace 2-frame music segments by speech0 if it is between speech
        i = 1
        while i <= len(l_smooth2) - 3:
            if l_smooth2[i - 1] == 2 and l_smooth2[i] + l_smooth2[i + 1] <= 1 and l_smooth2[i + 2] == 2:
                l_smooth3[i] = 2
                l_smooth3[i + 1] = 2
                i += 1
            else:
                l_smooth3[i] = l_smooth2[i]
            i += 1

        # add 1-frames bounds to allspeech intervals
        l_smooth4 = np.zeros(len(l_smooth3))
        for i in range(1, len(l_smooth3) - 1):
            if l_smooth3[i] == 2 and l_smooth3[i - 1] != 2:
                l_smooth4[i - 1] = 2
            l_smooth4[i] = l_smooth3[i]
            if l_smooth3[i] != 2 and l_smooth3[i - 1] == 2:
                l_smooth4[i] = 2
        mask = np.zeros(l_smooth4.shape[0])
        for i in range(len(mask)):
            mask[i] = int(l_smooth4[i] == 2)
        return mask

    def get_separation(self, features, scenes_mask, mask, clusters, method):
        if method == 'hmm':
            compressed_features = pca(n_components=self.n_components).fit(
                features[scenes_mask * mask == 1, :]).transform(features)
            h = hmm.GaussianHMM(n_components=clusters).fit(compressed_features[scenes_mask * mask == 1, :])
            labels = h.predict(compressed_features)
            labels[mask == 0] = -1
            return labels

    def get_audio_by_label(self, wav, rate, t, labels, label):
        new_wav = np.zeros(len(wav))
        for i in range(len(labels)):
            i0 = int((t[i] + self.step / 2.) * rate)
            i1 = int((t[i] + 3. * self.step / 2.) * rate)
            if labels[i] == label:
                new_wav[i0:i1] = wav[i0:i1]
            else:
                new_wav[i0:i1] = np.zeros(i1 - i0)
        return new_wav

    @staticmethod
    def get_mask_from_scenes(t, scenes):
        mask = np.zeros(t.shape)
        for scene in scenes:
            idx = (t >= scene[0]) * (t <= scene[1])
            mask[idx] = 1
        return mask

    def process(self, audio_path, fragments, out_path, n_clusters):
        audio_name = '.'.join(audio_path.split('/')[-1].split('.')[:-1])
        # out_path = out_path if out_path[-1] == '/' else out_path + '/'

        wav, rate = self.read_wav(audio_path)
        x, t = self.get_sample(wav, rate)
        probabilities = self.clear_model.process_data(x)
        embedding = self.embed_model.process_data(x)
        mask = self.get_mask(probabilities)
        # match mask and audio_mask
        if fragments is None:
            fragments = [[t[0], t[-1]]]

        scenes_mask = self.get_mask_from_scenes(t, fragments)

        if not isinstance(n_clusters, (tuple, list, np.ndarray)):
            n_clusters = [n_clusters]
        trash_wav = self.get_audio_by_label(wav, rate, t, mask, 0)


        out_path = osp.join(out_path, 'voices')
        if not osp.exists(out_path):
            os.mkdir(out_path)

        self.write_wav(osp.join(out_path, audio_name + '_trash.wav'), trash_wav, rate)
        for nc in n_clusters:
            labels = self.get_separation(embedding, scenes_mask, mask, nc, 'hmm')

            out_cl_path = osp.join(out_path, str(nc))
            if not osp.exists(out_cl_path):
                os.mkdir(out_cl_path)

            for n in range(nc):
                wav_i = self.get_audio_by_label(wav, rate, t, labels, n)
                self.write_wav(osp.join(out_cl_path, audio_name + '_voice_' + str(n) + '.wav'), wav_i, rate)
