"""
NeurodataLab LLC 23.01.2018
Created by Andrey Belyaev
"""
import csv
import json


def convert(in_csv, out_json, template_json=None):
    assert template_json is not None
    reader = csv.reader(open(in_csv, 'r'))
    lines = [l for l in reader]
    lines = {l[0]: l[1] for l in lines[1:]}

    template = json.load(open(template_json, 'r'))
    corr = list(set([(lines[k], template[k]) for k in lines.keys() if k in template]))
    corr = {c[0]: c[1] for c in corr}
    print corr
    lines = {k: corr[v] for k, v in lines.items() if v in corr}
    print set(map(lambda l: lines[l], lines.keys()))
    json.dump(lines, open(out_json, 'w'), indent=2)


if __name__ == '__main__':
    convert('/home/mopkobka/Downloads/Telegram Desktop/sides_final.csv',
            '/home/mopkobka/NDL-Projects/autonomous_fulltracker/DATA/sides.json',
            '/home/mopkobka/Downloads/Telegram Desktop/video_sides.json')
