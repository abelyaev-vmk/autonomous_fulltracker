"""
NeurodataLab LLC 22.01.2018
Created by Andrey Belyaev
"""
import cv2
import json
import os
import os.path as osp
from tqdm import tqdm

home = osp.dirname(osp.abspath(__file__))
simple_lists_path = '/run/user/1000/gvfs/smb-share:server=bigmax,share=backup/FINAL_VIDEO_TMP/_SIMPLE_LIST/'
video_paths = '/run/user/1000/gvfs/smb-share:server=bigmax,share=backup/FINAL_VIDEO_TMP/_ALL_RAW_VIDEO/'
video_sides = json.load(open('/home/mopkobka/NDL-Projects/autonomous_fulltracker/DATA/sides.json', 'r'))


def prepare_simple_lists():
    sl = {}
    for fold in tqdm([f for f in os.listdir(simple_lists_path) if '.' not in f]):
        for file_name in [f for f in os.listdir(osp.join(simple_lists_path, fold)) if f.endswith('.txt')]:
            spl = file_name.split('.')
            video_name = '.'.join(spl[:-2])
            video_type = spl[-2]
            sl[video_name] = [osp.join(fold, '%s.%s' % (video_name, video_type)), osp.join(fold, file_name)]
    return sl


def prepare_json(in_path, out_path, already_done_path=None):
    already_done_data = json.load(open(already_done_path, 'r')) if osp.exists(already_done_path) else {}
    already_done_data = {d['video_name']: d for d in already_done_data}
    sl = prepare_simple_lists()
    out_data = []
    in_json = json.load(open(in_path, 'r'))
    for n, video_info in enumerate(in_json):
        try:
            video_name = video_info['Name']
            if video_name in already_done_data:
                out_data.append(already_done_data[video_name])
                print 'already done', video_name
                continue
            if video_name not in sl:
                print 'not in the simple list', video_name
                continue

            fragments_sec = list(map(lambda vi: map(float, vi.split(',')), video_info['Csv'].strip().split('\n')[1:]))
            with open(osp.join(simple_lists_path, sl[video_name][1]), 'r') as f:
                scenes = list(map(lambda line: map(int, line.strip().split(',')), f.readlines()))

            v = cv2.VideoCapture(osp.join(video_paths, sl[video_name][0]))
            fps = v.get(cv2.CAP_PROP_FPS)

            out_data.append({
                'FID': video_info['FID'],
                'VID': video_info['VID'],
                'video_name': video_name,
                'video_path': sl[video_name][0],
                'fragments': fragments_sec,
                'fragments_type': 'sec',
                'scenes': scenes,
                'scenes_type': 'frame',
                'fps': fps,
                'side': video_sides[video_name]
            })
            print 'Add new', video_name
            if n % 100 == 0:
                print '%d\%d' % (n, len(in_json))
                with open(out_path, 'w') as f:
                    json.dump(out_data, f, indent=2)
        except:
            print 'bad video', video_name
            pass

    with open(out_path, 'w') as f:
        json.dump(out_data, f, indent=2)


def check_marking(marking_path):
    sl = prepare_simple_lists()
    marking = json.load(open(marking_path, 'r')) if osp.exists(marking_path) else {}
    print 1


if __name__ == '__main__':
    in_json_path = osp.join(home, '../DATA/start_end.json')
    out_json_path = osp.join(home, '../DATA/VIDEOS_INFO_final.json')
    prepare_json(in_json_path, out_json_path, osp.join(home, '../DATA/VIDEOS_INFO_2_2.json'))
    # check_marking(out_json_path)
