"""
NeurodataLab LLC 12.02.2018
Created by Andrey Belyaev
"""
import json
import os
import os.path as osp
import time
import yaml
from FaceDetector.model import init_model
from easydict import EasyDict as edict
from subprocess import call
from tqdm import tqdm

dev_null = open('/dev/null', 'w')


def detect_faces(images_dir):
    pass


def parse_images(video_file, img_nums, out_template):
    if len(img_nums) == 0:
        return

    eq_string = ""
    for img_num in img_nums:
        eq_string += "eq(n\,%d)+" % img_num
    eq_string = "select='%s'" % eq_string[:-1]

    call(['ffmpeg', '-i', video_file, '-vsync', 'vfr', '-q:v', '2', '-filter:v',
          'scale=640:380,%s,setpts=PTS-STARTPTS' % eq_string, out_template], stdout=dev_null, stderr=dev_null)


if __name__ == '__main__':
    config_path = '/home/mopkobka/NDL-Projects/autonomous_fulltracker/config.yml'
    videos_info_path = '/home/mopkobka/NDL-Projects/autonomous_fulltracker/DATA/VIDEOS_INFO.json'
    out_path = '/SSD/VideoScenesImages'

    with open(config_path, 'r') as f:
        config = edict(yaml.load(f))
    with open(videos_info_path, 'r') as f:
        videos_info = json.load(f)

    data_path = config.GLOBAL.VIDEO_PATHS
    config.GLOBAL.TMP_DIR = osp.join(out_path, 'marking')
    if not osp.exists(osp.join(out_path, 'imgs')):
        os.mkdir(osp.join(out_path, 'imgs'))

    process_video = init_model(config)

    for video_info in tqdm(videos_info):
        try:
            tmp_out = osp.join(out_path, 'imgs', video_info['video_name'])
            if osp.exists(tmp_out):
                continue
            os.mkdir(tmp_out)
            os.mkdir(osp.join(out_path, 'marking', video_info['video_name']))
            mul = video_info['fps'] if video_info['scenes_type'] == 'sec' else 1
            frames_to_detect = sorted(list(map(lambda s: int((s[1] + s[0]) * 0.33), video_info['scenes'])) +
                                      list(map(lambda s: int((s[1] + s[0]) * 0.50), video_info['scenes'])) +
                                      list(map(lambda s: int((s[1] + s[0]) * 0.80), video_info['scenes'])))
            parse_images(osp.join(data_path, video_info['video_path']),
                         frames_to_detect,
                         osp.join(tmp_out, video_info['video_name'] + '_%05d.jpg'))
            process_video(video_info['video_name'], tmp_out)
        except:
            pass
