"""
NeurodataLab LLC 13.02.2018
Created by Andrey Belyaev
"""
import json
import os
import os.path as osp
from subprocess import call
from tqdm import tqdm


if __name__ == '__main__':
    data_path = '/RAID/_ALL_RAW_VIDEO'
    out_path = '/RAID/VideoWavs'
    videos_info_path = '/home/mopkobka/NDL-Projects/autonomous_fulltracker/DATA/VIDEOS_INFO.json'
    with open(videos_info_path, 'r') as f:
        videos_info = json.load(f)
    dev_null = open('/dev/null', 'w')

    for video_info in tqdm(videos_info):
        ret = 1
        while ret != 0:
            out_wav_path = osp.join(out_path, video_info['video_name'] + '.wav')
            if osp.exists(out_wav_path):
                os.remove(out_wav_path)
            ret = call(['ffmpeg', '-i', osp.join(data_path, video_info['video_path']), out_wav_path],
                       stdout=dev_null, stderr=dev_null)
        break
