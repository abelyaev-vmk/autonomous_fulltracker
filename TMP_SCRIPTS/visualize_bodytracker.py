"""
NeurodataLab LLC 29.01.2018
Created by Andrey Belyaev
"""
import cv2
import numpy as np
import os
import os.path as osp


colors = [
    (0, 0, 255),
    (0, 255, 0),
    (255, 0, 0),
    (0, 255, 255),
    (255, 0, 255),
    (255, 255, 0)
]


def visualize_body_tracker(keypoints_path, data_path):
    body_tracker_feats = np.load(keypoints_path).tolist()
    images_list = sorted(os.listdir(data_path), key=lambda p: float(p.split('_')[-1].split('.')[0]))
    for im_path in images_list[100:]:
        image1 = cv2.imread(osp.join(data_path, im_path))
        image2 = image1.copy()
        keypoints = body_tracker_feats[im_path]
        for n, keypoint in enumerate(keypoints):
            if not 0 <= n < 18:
                continue
            for kp in keypoint:
                if kp[2] > 0.35:
                    cv2.putText(image1, str(n), (kp[0] + 3, kp[1] - 3), cv2.FONT_HERSHEY_COMPLEX, 0.3, colors[n % len(colors)])
                    cv2.circle(image1, kp[:2], 2, colors[n % len(colors)], 2)
        cv2.imshow('image1', image1)

        x_center = image2.shape[1] // 2
        left_color, right_color = (255, 0, 0), (0, 0, 255)
        for n, keypoint in enumerate(keypoints):
            if not 0 <= n < 18:
                continue
            keypoint = filter(lambda kp: kp[2] > 0.35, keypoint)
            if len(keypoint) == 0:
                continue
            elif len(keypoint) == 1:
                cv2.circle(image2, keypoint[0][:2], 2, left_color if keypoint[0][0] < x_center else right_color, 2)
            else:
                left_keypoints = filter(lambda kp: kp[0] < x_center, keypoint)
                right_keypoints = filter(lambda kp: kp[0] >= x_center, keypoint)
                if len(left_keypoints) > 0:
                    left_keypoint = sorted(left_keypoints, key=lambda kp: kp[2])[-1]
                    cv2.circle(image2, left_keypoint[:2], 2, left_color, 2)
                if len(right_keypoints) > 0:
                    right_keypoint = sorted(right_keypoints, key=lambda kp: kp[2])[-1]
                    cv2.circle(image2, right_keypoint[:2], 2, right_color, 2)

        cv2.imshow('image2', image2)

        if cv2.waitKey(0) & 0xff == ord('q'):
            break


if __name__ == '__main__':
    body_tracker_keypoints_path = '/home/mopkobka/NDL-Projects/autonomous_fulltracker/_TMP_DIR/_Wendy119_TEMP/_Wendy119_body-keypoints.npy'
    images_dir = '/home/mopkobka/NDL-Projects/autonomous_fulltracker/_TMP_DIR/_Wendy119_TEMP/images'
    visualize_body_tracker(body_tracker_keypoints_path, images_dir)
