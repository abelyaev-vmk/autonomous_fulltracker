"""
NeurodataLab LLC 25.01.2018
Created by Andrey Belyaev
"""

import json

import numpy as np

from VoiceSeparator.OLD.wavreadwrite import read_wave

wave_path = '/home/mopkobka/NDL-Projects/autonomous_fulltracker/_TMP_DIR/_national_in_deepth83/_national_in_deepth83.wav'
frag_path = '/home/mopkobka/NDL-Projects/autonomous_fulltracker/_TMP_DIR/_national_in_deepth83/fragments_info.json'

with open(frag_path, 'r') as f:
    fps, fragmetns = json.load(f)

x, fs = read_wave(wave_path)
print x.shape

sec = np.array(fragmetns, dtype=float) / fps

idxs = []
for s in sec:
    idxs += list(range(int(s * fs), int(s * fs + fs / fps)))
print len(idxs), len(list(set(idxs)))
print len(x)