"""
NeurodataLab LLC 22.01.2018
Created by Andrey Belyaev
"""
import json


video_info_path = '/home/mopkobka/NDL-Projects/autonomous_fulltracker/DATA/VIDEOS_INFO.json'
frag_left_border_sec = 4
scene_left_border_frames = 3


if __name__ == '__main__':
    for video_info in json.load(open(video_info_path, 'r')):
        fragments, scenes = video_info['fragments'], video_info['scenes']
        if video_info['fragments_type'] == 'sec':
            fragments = list(map(lambda frag: map(lambda f: int(f * video_info['fps']), frag), fragments))
        if video_info['scenes'] == 'sec':
            scenes = list(map(lambda scene: map(lambda sc: int(sc * video_info['fps']), scene), scenes))

        frag_left_border_frames = int(frag_left_border_sec * video_info['fps'])
        cur_scene_num = 0
        cur_scene = scenes[cur_scene_num]

        new_fragments = []
        add_frag = 0
        for frag in fragments:
            while not cur_scene[0] < frag[0] < cur_scene[1]:
                cur_scene_num += 1
                if cur_scene_num >= len(scenes):
                    cur_scene = None
                    break
                cur_scene = scenes[cur_scene_num]
                add_frag = 0
            if cur_scene is None:
                break
            new_fragments.append([max(cur_scene[0] + scene_left_border_frames, frag[0] - frag_left_border_frames), frag[1]])
            add_frag += 1
            if add_frag > 4:
                print video_info['video_name']

    print fragments
    print scenes
    print
    print new_fragments
    print len(new_fragments)
