"""
NeurodataLab LLC 31.01.2018
Created by Andrey Belyaev
"""
import cv2
import numpy as np
from time import time, sleep
from multiprocessing import Pool
from skimage.filters import gaussian
from threading import Thread


def get_keypoints_from_heatmap(heat_map):
    M = np.abs(heat_map).max()
    if M >= 1:
        heat_map /= M
    hmap = gaussian(heat_map, sigma=1)

    map_left = np.zeros(hmap.shape)
    map_left[1:, :] = hmap[:-1, :]
    map_right = np.zeros(hmap.shape)
    map_right[:-1, :] = hmap[1:, :]
    map_up = np.zeros(hmap.shape)
    map_up[:, 1:] = hmap[:, :-1]
    map_down = np.zeros(hmap.shape)
    map_down[:, :-1] = hmap[:, 1:]

    peaks_binary = np.logical_and.reduce(
        (hmap >= map_left, hmap >= map_right, hmap >= map_up, hmap >= map_down, hmap > 0.1))
    peaks = zip(np.nonzero(peaks_binary)[1].tolist(), np.nonzero(peaks_binary)[0].tolist())  # note reverse
    peaks_with_score = [x + (heat_map[x[1], x[0]],) for x in peaks]

    return peaks_with_score


class AsyncDict:
    def __init__(self, foo):
        self.foo = foo
        self.dict = {}
        self.pool = Pool(processes=2)
        self.processes = {}

    def add_item(self, key, arg):
        # self.dict[key] = self.foo(*args)

        self.processes[key] = self.pool.apply_async(self.foo, (arg, ))
        print 'Add item ok'

    def foo_map(self, arg):
        return map(self.foo, arg)

    def to_dict(self):
        self.__wait_processes()
        return self.dict

    def parse_processes(self):
        ready_keys = []
        for key, proc in self.processes.items():
            if proc.ready():
                ready_keys.append(key)
        for key in ready_keys:
            self.dict[key] = self.processes.pop(key).get()

    def __wait_processes(self):
        while len(self.processes) > 0:
            self.parse_processes()


def sleep_func(a):
    sleep(2)
    return a


def max_func(arr):
    return arr.max()


if __name__ == '__main__':
    big_arr = np.zeros((18, 150, 85))
    start = time()
    for _ in range(320):
        get_keypoints_from_heatmap(big_arr)
    print 'Alone:', time() - start

    ad = AsyncDict(get_keypoints_from_heatmap)
    start = time()
    for _ in range(320):
        ad.add_item('1', big_arr)
    print ad.to_dict()
    print 'Async', time() - start