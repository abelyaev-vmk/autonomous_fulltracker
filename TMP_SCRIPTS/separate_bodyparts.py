"""
NeurodataLab LLC 01.02.2018
Created by Andrey Belyaev
"""
import cv2
import numpy as np
import os
import os.path as osp
from cython.parse_keypoints import parse_keypoints


limbSeq = [[2, 3], [2, 6], [3, 4], [4, 5], [6, 7], [7, 8], [2, 9], [9, 10],
           [10, 11], [2, 12], [12, 13], [13, 14], [2, 1], [1, 15], [15, 17],
           [1, 16], [16, 18], [3, 17], [6, 18]]

mapIdx = [[31, 32], [39, 40], [33, 34], [35, 36], [41, 42], [43, 44], [19, 20], [21, 22],
          [23, 24], [25, 26], [27, 28], [29, 30], [47, 48], [49, 50], [53, 54], [51, 52],
          [55, 56], [37, 38], [45, 46]]


def separate_body_parts(maps, img):
    heat_map, limb_map = maps
    keypoints = parse_keypoints(heat_map)
    limb_map = cv2.resize(limb_map.transpose((1, 2, 0)), (0, 0), fx=4, fy=4)

    # for i in range(38):
    #     lm = limb_map[:, :, i].copy()
    #     lm = (lm - lm.min()) / (lm.max() - lm.min())
    #     cv2.imwrite(osp.join('/home/mopkobka/NDL-Projects/autonomous_fulltracker/TMP_SCRIPTS/limb_maps', '%d.jpg' % i),
    #                 (lm * 255).astype(int))

    connection_all = []
    special_k = []
    special_non_zero_index = []
    mid_num = 11

    param = {'thre2': 0.05}

    for k in range(len(mapIdx)):
        score_mid = limb_map[:, :, [x - 19 for x in mapIdx[k]]]
        # print score_mid.max()
        candA = keypoints[limbSeq[k][0] - 1]
        candB = keypoints[limbSeq[k][1] - 1]

        # print candA, candB

        nA = len(candA)
        nB = len(candB)
        indexA, indexB = limbSeq[k]
        if (nA != 0 and nB != 0):
            connection_candidate = []
            for i in range(nA):
                for j in range(nB):
                    try:
                        vec = np.subtract(candB[j][:2], candA[i][:2])
                        # print('vec: ',vec)
                        norm = np.sqrt(vec[0] * vec[0] + vec[1] * vec[1])
                        # print('norm: ', norm)
                        vec = np.divide(vec, norm)
                        # print('normalized vec: ', vec)
                        startend = zip(np.linspace(candA[i][0], candB[j][0], num=mid_num),
                                       np.linspace(candA[i][1], candB[j][1], num=mid_num))
                        # print('startend: ', startend)
                        vec_x = np.array([score_mid[int(round(startend[I][1])), int(round(startend[I][0])), 0]
                                          for I in range(len(startend))])
                        # print('vec_x: ', vec_x)
                        vec_y = np.array([score_mid[int(round(startend[I][1])), int(round(startend[I][0])), 1]
                                          for I in range(len(startend))])
                        # print('vec_y: ', vec_y)
                        score_midpts = np.multiply(vec_x, vec[0]) + np.multiply(vec_y, vec[1])
                        # print(score_midpts)
                        # print('score_midpts: ', score_midpts)
                        score_with_dist_prior = sum(score_midpts) / len(score_midpts) + min(
                            0.5 * img.shape[0] / norm - 1, 0)

                        # print('score_with_dist_prior: ', score_with_dist_prior)
                        criterion1 = len(np.nonzero(score_midpts > param['thre2'])[0]) > 0.8 * len(score_midpts)
                        # print('score_midpts > param["thre2"]: ', len(np.nonzero(score_midpts > param['thre2'])[0]))
                        criterion2 = score_with_dist_prior > 0

                        if criterion1 and criterion2:
                            # print('match')
                            # print(i, j, score_with_dist_prior, score_with_dist_prior+candA[i][2]+candB[j][2])
                            connection_candidate.append(
                                [i, j, score_with_dist_prior, score_with_dist_prior + candA[i][2] + candB[j][2]])
                    except:
                        pass
                        # print 'error rendering'
                        # print('--------end-----------')
            connection_candidate = sorted(connection_candidate, key=lambda x: x[2], reverse=True)
            # print('-------------connection_candidate---------------')
            # print(connection_candidate)
            # print('------------------------------------------------')
            connection = np.zeros((0, 5))
            for c in range(len(connection_candidate)):
                i, j, s = connection_candidate[c][0:3]
                if (i not in connection[:, 3] and j not in connection[:, 4]):
                    connection = np.vstack([connection, [candA[i][3], candB[j][3], s, i, j]])
                    # print('----------connection-----------')
                    # print(connection)
                    # print('-------------------------------')
                    if (len(connection) >= min(nA, nB)):
                        break

            connection_all.append(connection)
            # elif(nA != 0 or nB != 0):
        else:
            special_k.append(k)
            special_non_zero_index.append(indexA if nA != 0 else indexB)
            connection_all.append([])

    subset = -1 * np.ones((0, 20))

    candidate = np.array([item for sublist in keypoints for item in sublist])


    for k in range(len(mapIdx)):
        if k not in special_k:
            try:
                partAs = connection_all[k][:, 0]
                partBs = connection_all[k][:, 1]
                indexA, indexB = np.array(limbSeq[k]) - 1

                for i in range(len(connection_all[k])):  # = 1:size(temp,1)
                    found = 0
                    subset_idx = [-1, -1]
                    for j in range(len(subset)):  # 1:size(subset,1):
                        if subset[j][indexA] == partAs[i] or subset[j][indexB] == partBs[i]:
                            subset_idx[found] = j
                            found += 1

                    if found == 1:
                        j = subset_idx[0]
                        if (subset[j][indexB] != partBs[i]):
                            subset[j][indexB] = partBs[i]
                            subset[j][-1] += 1
                            subset[j][-2] += candidate[partBs[i].astype(int), 2] + connection_all[k][i][2]
                    elif found == 2:  # if found 2 and disjoint, merge them
                        j1, j2 = subset_idx
                        # print "found = 2"
                        membership = ((subset[j1] >= 0).astype(int) + (subset[j2] >= 0).astype(int))[:-2]
                        if len(np.nonzero(membership == 2)[0]) == 0:  # merge
                            subset[j1][:-2] += (subset[j2][:-2] + 1)
                            subset[j1][-2:] += subset[j2][-2:]
                            subset[j1][-2] += connection_all[k][i][2]
                            subset = np.delete(subset, j2, 0)
                        else:  # as like found == 1
                            subset[j1][indexB] = partBs[i]
                            subset[j1][-1] += 1
                            subset[j1][-2] += candidate[partBs[i].astype(int), 2] + connection_all[k][i][2]

                    # if find no partA in the subset, create a new subset
                    elif not found and k < 17:
                        row = -1 * np.ones(20)
                        row[indexA] = partAs[i]
                        row[indexB] = partBs[i]
                        row[-1] = 2
                        row[-2] = sum(candidate[connection_all[k][i, :2].astype(int), 2]) + connection_all[k][i][2]
                        subset = np.vstack([subset, row])
            except:
                pass
                # print "not link"

    # delete some rows of subset which has few parts occur
    # print 'subset was', len(subset)  # , subset
    deleteIdx = []
    for i in range(len(subset)):
        if subset[i][-1] < 4 or subset[i][-2] / subset[i][-1] < 0.4:
            deleteIdx.append(i)
    subset = np.delete(subset, deleteIdx, axis=0)
    # print 'subset now', len(subset)  # , subset

    colors = [[255, 0, 0], [255, 85, 0], [255, 170, 0], [255, 255, 0], [170, 255, 0], [85, 255, 0], [0, 255, 0], \
              [0, 255, 85], [0, 255, 170], [0, 255, 255], [0, 170, 255], [0, 85, 255], [0, 0, 255], [85, 0, 255], \
              [170, 0, 255], [255, 0, 255], [255, 0, 170], [255, 0, 85]]
    import matplotlib
    import math

    # canvas = img.copy()  # B,G,R order
    #
    # for i in range(18):
    #     for j in range(len(keypoints[i])):
    #         cv2.circle(canvas, keypoints[i][j][0:2], 4, colors[i], thickness=-1)

    # to_plot = cv2.addWeighted(img, 0.3, canvas, 0.7, 0)
    # plt.imshow(to_plot[:, :, [2, 1, 0]])
    # fig = matplotlib.pyplot.gcf()
    # fig.set_size_inches(12, 12)
    # candidate = np.array(sorted(candidate, key=lambda k: k[-1]))
    # visualize 2
    # stickwidth = 4
    # print len(subset)
    for i in range(17):
        for n in range(len(subset)):
            index = subset[n][np.array(limbSeq[i]) - 1]
            if -1 in index:
                continue
            cur_canvas = canvas.copy()
            Y = candidate[index.astype(int), 0]
            X = candidate[index.astype(int), 1]
            mX = np.mean(X)
            mY = np.mean(Y)
            length = ((X[0] - X[1]) ** 2 + (Y[0] - Y[1]) ** 2) ** 0.5
            angle = math.degrees(math.atan2(X[0] - X[1], Y[0] - Y[1]))
            polygon = cv2.ellipse2Poly((int(mY), int(mX)), (int(length / 2), stickwidth), int(angle), 0, 360, 1)
            cv2.fillConvexPoly(cur_canvas, polygon, colors[i])
            canvas = cv2.addWeighted(canvas, 0.4, cur_canvas, 0.6, 0)
    #
    # return canvas


def visualize_separated_body_parts(keypoints_path, data_path):
    body_tracker_info = np.load(keypoints_path).tolist()
    images_list = sorted(os.listdir(data_path))
    for img_path in images_list[50:]:
        image = cv2.imread(osp.join(data_path, img_path))
        cur_info = body_tracker_info[img_path]
        keypoints = cur_info['parsed']
        maps = cur_info['clear']

        start = time()
        separate_body_parts(maps, image)
        print time() - start

        # image2 = separate_body_parts(maps, image)
        # cv2.imshow('image2', image2)
        # if cv2.waitKey(0) & 0xff == ord('q'):
        #     break


if __name__ == '__main__':
    from time import time
    body_tracker_keypoints_path = '/home/mopkobka/NDL-Projects/autonomous_fulltracker/_TMP_DIR/_Colbert524/tmp_bodytracker.npyALL.npy'
    images_dir = '/home/mopkobka/NDL-Projects/autonomous_fulltracker/_TMP_DIR/_Colbert524/images'
    visualize_separated_body_parts(body_tracker_keypoints_path, images_dir)
