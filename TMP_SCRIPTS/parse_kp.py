"""
NeurodataLab LLC 24.01.2018
Created by Andrey Belyaev
"""

import numpy as np
from skimage.filters import gaussian

kp = np.load('/home/mopkobka/NDL-Projects/autonomous_fulltracker/_TMP_DIR/stevewilkos140/stevewilkos140_body-keypoints.npy').tolist()
img0 = kp[kp.keys()[0]]
heatmap_avg = img0['heat_map']

param = {}
param['octave'] = 3
param['use_gpu'] = 1
param['starting_range'] = 0.8
param['ending_range'] = 2
param['scale_search'] = [0.5, 1, 1.5, 2]
param['thre1'] = 0.1
param['thre2'] = 0.05
param['thre3'] = 0.5
param['mid_num'] = 4
param['min_num'] = 10
param['crop_ratio'] = 2.5
param['bbox_ratio'] = 0.25
param['GPUdeviceNumber'] = 1

all_peaks = []
peak_counter = 0

for part in range(19 - 1):
    x_list = []
    y_list = []
    map_ori = heatmap_avg[:, :, part]
    map = gaussian(map_ori, sigma=3)

    map_left = np.zeros(map.shape)
    map_left[1:, :] = map[:-1, :]
    map_right = np.zeros(map.shape)
    map_right[:-1, :] = map[1:, :]
    map_up = np.zeros(map.shape)
    map_up[:, 1:] = map[:, :-1]
    map_down = np.zeros(map.shape)
    map_down[:, :-1] = map[:, 1:]

    peaks_binary = np.logical_and.reduce(
        (map >= map_left, map >= map_right, map >= map_up, map >= map_down, map > param['thre1']))
    peaks = zip(np.nonzero(peaks_binary)[1], np.nonzero(peaks_binary)[0])  # note reverse
    # print peaks
    peaks_with_score = [x + (map_ori[x[1], x[0]],) for x in peaks]
    id = range(peak_counter, peak_counter + len(peaks))
    peaks_with_score_and_id = [peaks_with_score[i] + (id[i],) for i in range(len(id))]

    all_peaks.append(peaks_with_score_and_id)
    peak_counter += len(peaks)

print 1