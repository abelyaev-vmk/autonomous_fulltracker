"""
NeurodataLab LLC 11.01.2018
Created by Andrey Belyaev
"""
import cv2
import json
import mxnet as mx
import numpy as np
import os
import os.path as osp


class SceneChangeDetectorModel:
    def __init__(self, model_path, width, height, ctx, change_threshold,
                 data_names=('data_node1', 'data_node2'), label_names=('scene_change_label',), batch_size=8):
        self.model_path = model_path
        self.width, self.height = width, height
        self.data_names, self.label_names = data_names, label_names
        self.change_threshold = change_threshold
        self.batch_size = batch_size
        if isinstance(ctx, int):
            self.ctx = mx.gpu(ctx)
        elif isinstance(ctx, (tuple, list)):
            self.ctx = [mx.gpu(c) for c in ctx]
        else:
            self.ctx = ctx

        self.data_shapes = [(self.batch_size, 3, self.height, self.width)] * len(self.data_names)
        self.label_shapes = [(self.batch_size, 1)] * len(self.label_names)

        sym, arg_params, aux_params = mx.model.load_checkpoint(model_path, 0)
        self.model = mx.module.Module(symbol=sym, data_names=self.data_names, label_names=self.label_names,
                                      context=self.ctx)
        self.model.bind(data_shapes=zip(self.data_names, self.data_shapes),
                        label_shapes=zip(self.label_names, self.label_shapes), for_training=False)
        self.model.init_params(arg_params=arg_params, aux_params=aux_params)

        # zero forward
        self.model.forward(mx.io.DataBatch(data=[mx.nd.zeros(shape) for shape in self.data_shapes]), is_train=False)

    def predict(self, batch, paths, res):
        self.model.forward(mx.io.DataBatch(data=[mx.nd.array(batch[:, 0, :]), mx.nd.array(batch[:, 1, :])]))
        predictions = self.model.get_outputs()[0].asnumpy()
        for img, path, im_res in zip(batch[:, 0], paths, predictions):
            if path is not None:
                res[path] = float(im_res) < self.change_threshold

    def get_image(self, img_path):
        img = cv2.imread(img_path)
        img = cv2.resize(img, (self.width, self.height))
        img = img.astype(float) / 255.
        img = img.transpose((2, 0, 1))
        return img

    def process_video(self, data_path):
        images_paths = sorted(os.listdir(data_path))
        if len(images_paths) < 2:
            return {}
        batch, paths = [], []
        result = {}
        cur_img = self.get_image(osp.join(data_path, images_paths[0]))
        for img_path in images_paths[1:]:
            image = self.get_image(osp.join(data_path, img_path))
            batch.append(np.array([image, cur_img]))
            paths.append(img_path)
            cur_img = image

            if len(batch) < self.batch_size:
                continue

            self.predict(np.array(batch), paths, result)
            del batch, paths
            batch, paths = [], []

        if len(batch) > 0:
            for _ in range(len(batch), self.batch_size):
                batch.append(np.zeros_like(batch[0]))
                paths.append(None)
            self.predict(np.array(batch), paths, result)
        return result
