"""
NeurodataLab LLC 23.11.2017
Created by Andrey Belyaev

Face Tracker: clustering and encode
"""
import cv2
import json
import mxnet as mx
import numpy as np
import os
import os.path as osp
from clustering import cluster_faces_on_images, save_clusters


# FaceTracker
class FaceTrackerModel:
    def __init__(self, model_path, width, height, ctx, feat_layer_name,
                 max_feat_diff, max_missed_frames_count, batch_size=8):
        self.max_feat_diff = max_feat_diff
        self.max_missed_frames_count = max_missed_frames_count
        sym, arg_params, aux_params = mx.model.load_checkpoint(model_path, 0)
        self.target_size = width, height
        self.data_names = ['data']
        self.data_shapes = [(batch_size, 3, width, height)]
        self.batch_size = batch_size
        if isinstance(ctx, int):
            self.ctx = mx.gpu(ctx)
        elif isinstance(ctx, (tuple, list)):
            self.ctx = [mx.gpu(c) for c in ctx]
        else:
            self.ctx = ctx
        self.model = mx.module.Module(symbol=sym.get_internals()[feat_layer_name], data_names=self.data_names,
                                      label_names=None, context=self.ctx)
        self.model.bind(data_shapes=zip(self.data_names, self.data_shapes), for_training=False)
        self.model.init_params(arg_params=arg_params, aux_params=aux_params, allow_missing=True)

        # zero forward
        self.model.forward(mx.io.DataBatch(data=[mx.nd.zeros(self.data_shapes[0])]), is_train=False)

    def cluster_video(self, data_path, face_marking_path, scenes_changes, out_path, video_name, human_side='left'):
        with open(face_marking_path, 'r') as f:
            face_marking = json.load(f)
        images, paths, bboxes = [], [], []
        image_paths = sorted(face_marking.keys())
        embeddings = {}
        for image_path in image_paths:
            image = cv2.imread(osp.join(data_path, image_path))
            for face in face_marking[image_path]:
                x, y, w, h = map(int, (face['x'], face['y'], face['w'], face['h']))
                face_img = cv2.resize(image[y: y + h, x: x + w], self.target_size)
                if face_img.max() > 1:
                    face_img = face_img.astype(float) / 255.
                face_img = face_img.transpose((2, 0, 1))

                images.append(face_img)
                paths.append(image_path)
                bboxes.append((x, y, w, h))
                if len(images) == self.batch_size:
                    self.update_embeddings(embeddings, images, paths, bboxes)
                    del images, paths, bboxes
                    images, paths, bboxes = [], [], []

        if len(images) > 0:
            for _ in range(len(images), self.batch_size):
                images.append(np.zeros(self.data_shapes[0][1:]))
                paths.append(None)
                bboxes.append(None)
            self.update_embeddings(embeddings, images, paths, bboxes)

        clusters = cluster_faces_on_images(embeddings, data_path, scenes_changes,
                                           self.max_missed_frames_count, self.max_feat_diff, human_side)
        return save_clusters(clusters, out_path, video_name)

    def update_embeddings(self, embeddings, images, paths, bboxes):
        self.model.forward(mx.io.DataBatch(data=[mx.nd.array(images)]), is_train=False)
        feats = self.model.get_outputs()[0].asnumpy()
        for path, bbox, feat in zip(paths, bboxes, feats):
            if path is None:
                continue
            if path not in embeddings:
                embeddings[path] = []
            embeddings[path].append({'feat': feat, 'bbox': bbox})


# For debug
if __name__ == '__main__':
    model = FaceTrackerModel('/home/mopkobka/NDL-Projects/FullTracker_v2/PretrainedNets/face_embeddings',
                             250, 250, 0, 'feat_ident_output', 4)
    c = model.cluster_video('/home/mopkobka/NDL-Projects/FullTracker_v2/_TMP_DIR/_Chelsea12.06/images',
                            '/home/mopkobka/NDL-Projects/FullTracker_v2/_TMP_DIR/_Chelsea12.06/_Chelsea12.06_faces.json',
                            '/home/mopkobka/NDL-Projects/FullTracker_v2/_TMP_DIR/_Chelsea12.06',
                            '_Chelsea12.06')
    print c
