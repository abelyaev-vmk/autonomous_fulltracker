"""
NeurodataLab LLC 23.11.2017
Created by Andrey Belyaev

Clustering algorithm for face tracker
"""
import json
import numpy as np
import os.path as osp
from LipsMotion import FaceLipsMotion


def norm(v1, v2):
    return np.sqrt(((v1 - v2) ** 2).sum())


def to_json_bbox(bbox):
    return {'x': bbox[0], 'y': bbox[1], 'w': bbox[2], 'h': bbox[3]}


# Cluster for unique person
class Cluster:
    def __init__(self, init_feat, init_path, init_bbox, data_path):
        self.feats = [init_feat]
        self.paths = [init_path]
        self.bboxes = [init_bbox]
        self.center = init_feat
        self.lips_motion = FaceLipsMotion(data_path)

        self.lips_motion.process_frame(init_path, init_bbox)

    @property
    def n_feats(self):
        return len(self.feats)

    def add_feat(self, feat, path, bbox):
        self.center = self.center * (self.n_feats / (self.n_feats + 1)) + feat / (self.n_feats + 1)
        self.feats.append(feat)
        self.paths.append(path)
        self.bboxes.append(bbox)
        self.lips_motion.process_frame(path, bbox)

    @staticmethod
    def iou(boxA, boxB):
        xA = max(boxA[0], boxB[0])
        yA = max(boxA[1], boxB[1])
        xB = min(boxA[2], boxB[2])
        yB = min(boxA[3], boxB[3])

        interArea = (xB - xA + 1) * (yB - yA + 1)

        boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
        boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)
        try:
            iou = interArea / float(boxAArea + boxBArea - interArea)
        except ZeroDivisionError:
            return 0
        else:
            return iou

    def last_bbox_dist(self, bbox):
        prev_bbox = self.bboxes[-1]
        # return self.iou(prev_bbox, bbox)
        return abs(prev_bbox[0] - bbox[0]) + abs(prev_bbox[1] - bbox[1])

    def dist(self, feat, scene_changed=True, bbox=None):
        if scene_changed or bbox is None:
            return norm(self.center, feat)
        else:
            bbox_dist = self.last_bbox_dist(bbox)
            if bbox_dist > 30:
                return norm(self.center, feat)
            else:
                return bbox_dist / 30.
        # return np.mean(lambda f: norm(f, feat), self.feats)

    @property
    def json_like(self):
        json_out = {}
        for path, bbox in zip(self.paths, self.bboxes):
            json_out[path] = {}
            json_out[path].update(to_json_bbox(bbox))
            json_out[path].update({'speak': self.lips_motion.get_speak_status(path)})
        return json_out

    @property
    def np_feats(self):
        return {path: feat for path, feat in zip(self.paths, self.feats)}


# Cluster all faces on video
def cluster_faces_on_images(info, data_path, scenes_changes, max_missed_frames_count=20, max_feat_diff=4, side='left'):
    def add_to_cluster(cluster, face):
        if cluster is None:
            cluster = Cluster(face['feat'], image_path, face['bbox'], data_path)
        else:
            cluster.add_feat(face['feat'], image_path, face['bbox'])
        return cluster

    image_paths = sorted(info.keys())
    clusters = []

    for image_path in image_paths:
        scene_changed = image_path in scenes_changes and scenes_changes[image_path]
        done_cluster_idxs = []
        for face in info[image_path]:
            dists = np.array([cluster.dist(face['feat'], scene_changed, face['bbox']) for cluster in clusters])
            if len(np.where(dists < max_feat_diff)[0]) == 0:
                clusters.append(Cluster(face['feat'], image_path, face['bbox'], data_path))
                done_cluster_idxs.append(len(clusters) - 1)
            else:
                dist_idxs = np.argsort(dists)
                found = False
                for idx in dist_idxs:
                    if int(idx) in done_cluster_idxs:
                        continue
                    else:
                        clusters[int(idx)].add_feat(face['feat'], image_path, face['bbox'])
                        found = True
                        done_cluster_idxs.append(idx)
                        break
                if not found:
                    clusters.append(Cluster(face['feat'], image_path, face['bbox'], data_path))
                    done_cluster_idxs.append(len(clusters) - 1)
    return clusters


# Save json-like ids information and numpy-like face embeddings
def save_clusters(clusters, out_path, video_name):
    out_persons_jsons = map(lambda c: osp.join(out_path, '%s_faces_id%d.json' % (video_name, c)), range(len(clusters)))
    out_persons_feats = map(lambda c: osp.join(out_path, '%s_faces_id%s.npy' % (video_name, c)), range(len(clusters)))
    for out_json, out_feat, cluster in zip(out_persons_jsons, out_persons_feats, clusters):
        np.save(out_feat, cluster.np_feats)
        with open(out_json, 'w') as f:
            json.dump(cluster.json_like, f, indent=2)
    return out_persons_jsons
