"""
NeurodataLab LLC 08.12.2017
Created by Andrey Belyaev
"""

import cv2
import dlib
import numpy as np
import os.path as osp


predictor = dlib.shape_predictor(osp.join(osp.dirname(osp.abspath(__file__)), 'data/shape_predictor.dat'))


def shape_to_np(shape, dtype="int"):
    coords = np.zeros((68, 2), dtype=dtype)
    for i in range(0, 68):
        coords[i] = (shape.part(i).x, shape.part(i).y)
    return coords


def dist(v1, v2):
    return np.sqrt(sum(map(lambda v1_, v2_: (v1_ - v2_) ** 2, v1, v2)))


class FaceLipsMotion:
    lip_idxs = (48, 54, 51, 57, 60, 64, 62, 66)

    def __init__(self, data_path, vec_length=30, trigger_pct=0.1, trigger_threshold=0.0035, silence_count=10):
        self.data_path = data_path
        self.vec_length = vec_length
        self.trigger_pct = trigger_pct
        self.trigger_threshold = trigger_threshold
        self.silence_count = silence_count

        self.vec = []
        self.statuses = {}

    def process_frame(self, im_path, bbox):
        image = cv2.imread(osp.join(self.data_path, im_path))
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        x, y, w, h = bbox
        rect = dlib.rectangle(x, y, x + w, y + h)
        dlib_shape = predictor(gray, rect)
        keypoints = shape_to_np(dlib_shape)

        w = dist(keypoints[self.lip_idxs[4]], keypoints[self.lip_idxs[5]])
        h = dist(keypoints[self.lip_idxs[6]], keypoints[self.lip_idxs[7]])
        ecc = np.sqrt(1 - (h / w) ** 2)

        self.add_ecc(ecc)
        self.statuses[im_path] = self.get_cur_status()

    def add_ecc(self, ecc):
        if len(self.vec) < self.vec_length:
            self.vec.append(ecc)
        else:
            self.vec = self.vec[1:]
            self.vec.append(ecc)
        self.check_silence()

    def check_silence(self):
        if len(self.vec) < self.silence_count:
            return
        speak_count = 0
        for i in range(1 - self.silence_count, -1):
            speak_count += 1 if abs(self.vec[i] - self.vec[i - 1]) > self.trigger_threshold else 0
        if speak_count == 0:
            self.vec = self.vec[-self.silence_count:]

    def get_cur_status(self):
        speak_count = 0
        for i in range(1, len(self.vec)):
            if abs(self.vec[i] - self.vec[i - 1]) > self.trigger_threshold:
                speak_count += 1
        return 1 if float(speak_count) / len(self.vec) > self.trigger_pct else 0, float(speak_count) / len(self.vec)

    def get_speak_status(self, im_path):
        return self.statuses[im_path]
