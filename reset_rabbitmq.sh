#!/usr/bin/env bash

bash ./kill_receivers.sh
rabbitmqctl stop_app
rabbitmqctl reset
rabbitmqctl start_app