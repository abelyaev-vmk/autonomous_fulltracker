"""
NeurodataLab LLC 08.02.2018
Created by Andrey Belyaev
"""
import cv2
import json
import os
import os.path as osp
import pickle
from features_instances import *


class Person:
    def __init__(self, frame_num=0, body=None, face_bbox=None, face_feat=None, eyes=None):
        self.last_frame = frame_num
        self.bodies = {frame_num: BodyKeypoints(body)}
        self.face_bboxes = {
            frame_num: FaceBBox(face_bbox['x'], face_bbox['y'],
                                face_bbox['w'], face_bbox['h'], face_feat) if face_bbox is not None else None
        }
        self.left_eyes = {frame_num: EyeFeatures(eyes['left'])}
        self.right_eyes = {frame_num: EyeFeatures(eyes['right'])}
        self.face_cluster = FaceCluster(face_feat, frame_num)

    def try_add(self, person):
        body_distance = self.bodies[self.last_frame].dist(person.bodies[person.last_frame])
        d_kp_idxs = np.where(body_distance > 0)[0]
        print 'Body distance', body_distance

        if len(np.where(body_distance == -1)[0]) < 3 \
                and len(d_kp_idxs) > 0 and np.sum(body_distance[d_kp_idxs]) < 0.2 * 600 * len(d_kp_idxs):
            self.merge_person(person)
            return True

        if len(np.where(body_distance == -1)[0]) >= 3 or np.sum(body_distance[d_kp_idxs]) > 20 * len(d_kp_idxs):
            return False

        bbox_distance = self.face_bboxes[self.last_frame].dist(person.face_bboxes[person.last_frame], by='bbox')
        print 'Bbox distance '
        if bbox_distance > 0.8:
            self.merge_person(person)
            return True
        if bbox_distance < 0.3:
            return False

        feat_distance = self.face_bboxes[self.last_frame].dist(person.face_bboxes[person.last_frame].feat, by='feat')
        print 'Feat distance'
        if feat_distance < 4:
            self.merge_person(person)
            return True

        print '\n'
        return False

    def merge_person(self, person):
        self.bodies.update(person.bodies)
        self.face_bboxes.update(person.face_bboxes)
        self.left_eyes.update(person.left_eyes)
        self.right_eyes.update(person.right_eyes)
        self.face_cluster.update(person.face_cluster)
        self.last_frame = person.last_frame

    def get_features_in_frame(self, frame_num):
        return {
            'body': self.bodies[frame_num],
            'face': self.face_bboxes[frame_num],
            'eyes': {'left': self.left_eyes[frame_num], 'right': self.right_eyes[frame_num]}
        }

    def serialize(self, to_json=True):
        assert to_json

        return {
            'bodies': {
                k: v.serialize(to_json=to_json) if v is not None else None for k, v in self.bodies.items()
            },
            'face_bboxes': {
                k: v.serialize(to_json=to_json) if v is not None else None for k, v in self.face_bboxes.items()
            },
            'left_eyes': {
                k: v.serialize(to_json=to_json) if v is not None else None for k, v in self.left_eyes.items()
            },
            'right_eyes': {
                k: v.serialize(to_json=to_json) if v is not None else None for k, v in self.right_eyes.items()
            },
            'face_cluster': self.face_cluster.serialize(to_json=to_json),
            'last_frame': self.last_frame
        }


class PersonsSet:
    def __init__(self):
        self.main_person = None
        self.second_person = None

    @property
    def main(self):
        return self.main_person

    @property
    def second(self):
        return self.second_person

    @property
    def count(self):
        return int(self.main_person is not None) + int(self.second_person is not None)

    def attach_main_person(self, person):
        if self.main_person is None:
            self.main_person = person
        else:
            self.main_person.merge_person(person)

    def attach_second_person(self, person):
        if self.second_person is None:
            self.second_person = person
        else:
            self.second_person.merge_person(person)

    def attach_with_side_prior(self, persons, main_side, only_main=False):
        centers = map(lambda p: p.bodies[p.last_frame].body_center, persons)
        persons_order = np.argsort(map(lambda c: c[0], centers))
        if main_side == 'left':
            self.attach_main_person(persons[persons_order[0]])
            if not only_main:
                self.attach_second_person(persons[persons_order[1]])
        if main_side == 'right':
            self.attach_main_person(persons[persons_order[-1]])
            if not only_main:
                self.attach_second_person(persons[persons_order[0]])

    def get_persons_in_frame(self, frame_num):
        return [
            None if self.main is None or frame_num not in self.main_person.bodies.keys() else self.main,
            None if self.second is None or frame_num not in self.second_person.bodies.keys() else self.second
        ]

    def serialize(self, to_json=True):
        assert to_json

        return {
            'main': self.main_person.serialize(to_json=to_json),
            'second': self.second_person.serialize(to_json=to_json)
        }


class Scene:
    def __init__(self, frame_num, scene_info):
        self.frame_num = frame_num

        self.bodies = scene_info['body']
        self.faces_bbox = {v[0]: v[1] for v in scene_info['face']}
        self.eyes = {v[0]: v[1] for v in scene_info['eyes']}
        self.face_feats = {v[0]: v[1] for v in scene_info['feat']}
        self.sec = scene_info['sec']

        self.persons = []
        self.process()

    def process(self):
        # print 'Bodies in scene', len(self.bodies)
        bodies = BodyKeypoints.clean(self.bodies)
        for n_body, person_body in enumerate(bodies):
            person_idx = person_body.find_corresponding_face(self.faces_bbox)

            person_face_bbox = self.faces_bbox[person_idx] if person_idx in self.faces_bbox else None
            person_face_feat = self.face_feats[person_idx] if person_idx in self.face_feats else None
            person_eyes = self.eyes[person_idx] if person_idx in self.eyes else {'left': None, 'right': None}

            self.persons.append(Person(self.frame_num, person_body, person_face_bbox, person_face_feat, person_eyes))


class VideoAnalyzer:
    def __init__(self, video_name='tmp', person_side='left', all_frames=None):
        self.video_name = video_name
        self.person_side = person_side
        self.all_frames = all_frames

        self.persons = PersonsSet()

    def process_scene(self, frame_num, scene_info):
        scene = Scene(frame_num, scene_info)
        count = len(scene.persons)

        # count == 0 => no changes
        # count == 1 => add to main
        # count == 2 => add with side prior
        # count >= 3 => add only main with side prior

        # print 'Found persons count', count

        if count == 1:
            self.persons.attach_main_person(scene.persons[0])

        if count > 1:
            if self.person_side in ('left', 'right'):
                self.persons.attach_with_side_prior(scene.persons, self.person_side, only_main=count > 2)

    def persons_on_frame(self, frame_num):
        persons = self.persons.get_persons_in_frame(frame_num)
        features = map(lambda p: p.get_features_in_frame(frame_num) if p is not None else None, persons)
        return persons, features

    @staticmethod
    def process_video(marking_path, video_name, person_side):

        with open(marking_path, 'r') as f:
            marking = pickle.load(f)

        marking_keys = sorted(marking.keys())
        va = VideoAnalyzer(video_name, person_side, marking_keys)

        for marking_key in marking_keys:
            value = marking[marking_key]
            va.process_scene(marking_key, value)

        return va

    def visualize(self, images_dir, out_dir, key_corresponding_path):
        # if not osp.exists(out_dir):
        #     os.mkdir(out_dir)
        # vis_out = osp.join(out_dir, 'visualize')

        vis_out = out_dir
        if not osp.exists(vis_out):
            os.mkdir(vis_out)

        with open(key_corresponding_path, 'r') as f:
            key_corr = json.load(f)
            key_corr = {int(k): v for k, v in key_corr.items()}

        for frame in self.all_frames:
            image_path = osp.join(images_dir, key_corr[frame])
            image = cv2.imread(image_path)
            persons, features = self.persons_on_frame(frame)
            for person, feature, name, color in zip(persons, features, ('main', 'second'), ((0, 0, 255), (255, 0, 0))):
                try:
                    image = feature['body'].draw(image, color)
                except:
                    pass

                try:
                    image = feature['face'].draw(image, color, name)
                except:
                    pass

            cv2.imwrite(osp.join(vis_out, 'frame_%05d.jpg' % frame), image)

    def serialize(self, out_dir, to_pickle=True, to_json=True):
        try:
            if to_pickle:
                with open(osp.join(out_dir, 'VideoAnalyzer.pckl'), 'w') as f:
                    pickle.dump(self, f)
        except:
            pass

        try:
            if to_json:
                json_info = {
                    'video_name': self.video_name,
                    'person_side': self.person_side,
                    'all_frames': self.all_frames,
                    'persons': self.persons.serialize(to_json=True)
                }

                with open(osp.join(out_dir, 'VideoAnalyzer.json'), 'w') as f:
                    json.dump(json_info, f, indent=2)
        except:
            pass