"""
NeurodataLab LLC 07.02.2018
Created by Andrey Belyaev
"""
import cv2
import json
import numpy as np
import os
import os.path as osp
import pickle
from features_instances import *
from video_analyzer import VideoAnalyzer
from tqdm import tqdm


def separate_by_person(data_path, results_path, visualize=True, side='left'):
    with open(osp.join(results_path, 'marking.pckl'), 'r') as f:
        marking = pickle.load(f)
    with open(osp.join(results_path, 'key_corresponding.json'), 'r') as f:
        key_corr = json.load(f)
        key_corr = {int(k): v for k, v in key_corr.items()}

    marking_keys = sorted(marking.keys())
    persons = []

    va = VideoAnalyzer()

    for marking_key in tqdm(marking_keys):
        value = marking[marking_key]
        va.process_scene(marking_key, value)

    for marking_key in marking_keys:
        image_path = osp.join(data_path, key_corr[marking_key])
        image = cv2.imread(image_path)
        persons, features = va.persons_on_frame(marking_key)
        for person, feature, name, color in zip(persons, features, ('main', 'second'), ((0, 0, 255), (255, 0, 0))):
            try:
                feature['body'].draw(image, color)
            except:
                pass

            try:
                feature['face'].draw(image, color, name)
            except:
                pass

        cv2.imshow('1', image)
        if cv2.waitKey(0) & 0xff == ord('q'):
            exit(0)

    print 1


if __name__ == '__main__':
    images_dir = '/SSD/FullTracker_out_dir/MIT_Leadership8/images'
    results_dir = '/RAID/FullTracker_Results/MIT_Leadership8'
    marking_path = osp.join(results_dir, 'marking.pckl')

    video_analyzer = VideoAnalyzer.process_video(marking_path, 'MIT_Leadership8', 'left')
    video_analyzer.visualize(images_dir, results_dir, osp.join(results_dir, 'key_corresponding.json'))
    video_analyzer.serialize(results_dir, to_pickle=False, to_json=True)

    # separate_by_person(images_dir, results_dir, visualize=True)