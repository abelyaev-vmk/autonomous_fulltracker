"""
NeurodataLab LLC 08.02.2018
Created by Andrey Belyaev
"""
import cv2
import numpy as np
from math import atan2, degrees


class FaceBBox:
    def __init__(self, x, y, w, h, feat, score=1., *args, **kwargs):
        self.x, self.y, self.w, self.h, self.feat = int(x), int(y), int(w), int(h), feat
        self.score = score

    def to_list(self, with_score=False):
        ret = [self.x, self.y, self.w, self.h, self.feat]
        if with_score:
            ret.append(self.score)
        return ret

    def to_dict(self, with_score=False):
        ret = {'x': self.x, 'y': self.y, 'w': self.w, 'h': self.h, 'feat': self.feat}
        if with_score:
            ret['score'] = self.score
        return ret

    def bbox_dist(self, bbox):
        bbox = self.prepare_bbox(bbox)
        return self.iou((self.x, self.y, self.x + self.w, self.y + self.h),
                        (bbox.x, bbox.y, bbox.x + bbox.w, bbox.y + bbox.h))

    def feat_dist(self, feat):
        if self.feat is not None:
            return np.sqrt(((np.array(feat) - np.array(self.feat)) ** 2).sum())
        else:
            return -1

    def dist(self, feature, by='bbox'):
        # if feature is None:
        #     return -1

        if by == 'bbox':
            ret = self.bbox_dist(feature)
        elif by == 'feat':
            ret = self.feat_dist(feature)
        else:
            raise NotImplementedError('Cannot calculate face bbox distance by %s' % by)
        return ret

    def draw(self, img, color, name=None, thickness=1):
        cv2.rectangle(img, (self.x, self.y), (self.x + self.w, self.y + self.h), color, thickness)
        if name is not None:
            cv2.putText(img, str(name), (self.x, self.y - 6), cv2.FONT_HERSHEY_COMPLEX, 0.3, color)
        return img

    @staticmethod
    def iou(bbox1, bbox2):
        x1 = max(bbox1[0], bbox2[0])
        y1 = max(bbox1[1], bbox2[1])
        x2 = min(bbox1[2], bbox2[2])
        y2 = min(bbox1[3], bbox2[3])

        inter_area = (x2 - x1 + 1) * (y2 - y1 + 1)

        box1_area = (bbox1[2] - bbox1[0] + 1) * (bbox1[3] - bbox1[1] + 1)
        box2_area = (bbox2[2] - bbox2[0] + 1) * (bbox2[3] - bbox2[1] + 1)
        try:
            iou = inter_area / float(box1_area + box2_area - inter_area)
        except ZeroDivisionError:
            return 0
        else:
            return iou

    @staticmethod
    def prepare_bbox(face_bbox, face_feat=None):
        if isinstance(face_bbox, FaceBBox):
            face_bbox.feat = face_feat
            return face_bbox
        elif isinstance(face_bbox, (tuple, list)):
            return FaceBBox(*face_bbox, feat=face_feat)
        elif isinstance(face_bbox, np.ndarray):
            return FaceBBox(*face_bbox.tolist(), feat=face_feat)
        elif isinstance(face_bbox, dict):
            face_bbox['feat'] = face_feat
            return FaceBBox(**face_bbox)
        elif face_bbox is None:
            return None
        else:
            raise NotImplementedError('Unsupported face bbox type %s' % str(type(face_bbox)))

    def serialize(self, to_json=True):
        assert to_json
        return {'x': self.x, 'y': self.y, 'w': self.w, 'h': self.h, 'feat': self.feat, 'score': self.score}


class BodyKeypoints:
    head_keypoints_idxs = [0, -4, -3]  # add -2 and -1 for ears
    limb_seq = [[2, 3], [2, 6], [3, 4], [4, 5], [6, 7], [7, 8], [2, 9], [9, 10],
                [10, 11], [2, 12], [12, 13], [13, 14], [2, 1], [1, 15], [15, 17],
                [1, 16], [16, 18], [3, 17], [6, 18]]

    def __init__(self, keypoints, *args, **kwargs):
        self.keypoints = np.array(keypoints) if not isinstance(keypoints, BodyKeypoints) else keypoints.keypoints

    def find_corresponding_face(self, faces_with_idxs):
        def check_point(point, x0, y0, w0, h0):
            if point is None:
                return False
            x_, y_ = point[:2]
            return x0 <= x_ <= x0 + w0 and y0 <= y_ <= y + h0

        for face_idx, face in faces_with_idxs.items():
            x, y, w, h = map(int, (face['x'], face['y'], face['w'], face['h']))
            is_inside = True
            for point in self.keypoints[self.head_keypoints_idxs]:
                is_inside = is_inside and check_point(point, x, y, w, h)
            if is_inside:
                return face_idx
        return -1

    def dist(self, body):
        dists = []
        for n, keypoint in enumerate(self.keypoints):
            if np.logical_xor(keypoint is None, body.keypoints[n] is None):
                dists.append(-1)
            elif np.logical_and(keypoint is None, body.keypoints[n] is None):
                dists.append(0)
            else:
                cur_kp = np.array(keypoint[:2])
                new_kp = np.array(body.keypoints[n][:2])
                dists.append(np.sqrt(((cur_kp - new_kp) ** 2).sum()))
        return np.array(dists)

    def draw(self, img, color, radius=2, thickness=-1):
        body_img = img.copy()
        for i in range(17):
            try:
                idx1, idx2 = np.array(self.limb_seq[i]) - 1
                point1, point2 = self.keypoints[idx1], self.keypoints[idx2]
                if point1 is None or point2 is None:
                    continue

                point1, point2 = np.array(point1[:2]), np.array(point2[:2])
                m_x = np.mean((point1[0], point2[0]))
                m_y = np.mean((point1[1], point2[1]))
                length = ((point1 - point2) ** 2).sum() ** 0.5
                angle = degrees(atan2(point1[1] - point2[1], point1[0] - point2[0]))
                polygon = cv2.ellipse2Poly((int(m_x), int(m_y)), (int(length / 2), 4), int(angle), 0, 360, 1)
                cv2.fillConvexPoly(body_img, polygon, color)
            except:
                pass

        return cv2.addWeighted(img, 0.5, body_img, 0.5, 0)

    @property
    def body_center(self):
        n, center = 0, np.array([0, 0], dtype=np.float)
        for kp in self.keypoints:
            if kp is not None:
                center += np.array(kp[:2])
                n += 1
        return center / n

    @staticmethod
    def prepare_body_keypoints(body_keypoints):
        if isinstance(body_keypoints, BodyKeypoints):
            return body_keypoints
        elif isinstance(body_keypoints, (list, tuple, np.ndarray)):
            return BodyKeypoints(body_keypoints)
        elif body_keypoints is None:
            return None
        else:
            raise NotImplementedError('Unsupported body keypoints type %s' % str(type(body_keypoints)))

    @staticmethod
    def clean(body_keypoints):
        cleaned_body_keypoints = []
        for keypoints in body_keypoints:
            if len(keypoints) > 3:
                cleaned_body_keypoints.append(BodyKeypoints(keypoints))

        return cleaned_body_keypoints

    def serialize(self, to_json=True):
        assert to_json
        return [kp.tolist() if kp is not None else None for kp in self.keypoints.tolist()]


class EyeFeatures:
    def __init__(self, eye, *args, **kwargs):
        self.eye = eye

    @staticmethod
    def prepare_eye_features(eye_feats):
        if isinstance(eye_feats, EyeFeatures):
            return eye_feats
        elif isinstance(eye_feats, (list, tuple, np.ndarray)):
            return EyeFeatures(eye_feats)
        elif eye_feats is None:
            return None
        else:
            raise NotImplementedError('Unsupported eye features type %s' % str(type(eye_feats)))

    def serialize(self, to_json=True):
        assert to_json
        return self.eye if not isinstance(self.eye, np.ndarray) else self.eye.tolist()


class FaceCluster:
    def __init__(self, feat=None, frame_num=None):
        self.feats = {} if feat is None else {frame_num: np.array(feat)}
        self.center = None if feat is None else np.array(feat)
        self.last_frame = frame_num

    def append(self, feat, frame_num):
        if self.n_feats == 0:
            self.feats = {frame_num: np.array(feat)}
            self.center = np.array(feat)
            self.last_frame = frame_num
        else:
            self.feats[frame_num] = np.array(feat)
            self.recalculate_center()
            self.last_frame = frame_num

    def recalculate_center(self):
        self.center = self.center * (self.n_feats / (self.n_feats + 1.)) + \
                      self.feats[self.last_frame] / (self.n_feats + 1.)

    def dist(self, feat):
        return np.sqrt(((self.center - feat) ** 2).sum())

    @property
    def n_feats(self):
        return len(self.feats)

    def update(self, cluster):
        if cluster.n_feats > 0 and self.n_feats > 0:
            self.feats.update(cluster.feats)
            self.center = self.center * (self.n_feats / (self.n_feats + cluster.n_feats + 0.)) + \
                          cluster.center / (self.n_feats + cluster.n_feats + 0.)
            self.last_frame = cluster.last_frame
        elif cluster.n_feats > 0 and self.n_feats == 0:
            self.feats = cluster.feats
            self.center = cluster.center
            self.last_frame = cluster.last_frame

    def serialize(self, to_json=True):
        assert to_json

        return {
            'feats': {k: v.tolist() for k, v in self.feats.items()} if self.feats is not None else None,
            'center': self.center.tolist() if self.center is not None else None,
            'last_frame': self.last_frame
        }


class FrameFeatures:
    def __init__(self, face_bbox=None, face_feat=None, body_keypoints=None, left_eye_feats=None, right_eye_feats=None):
        self.face = FaceBBox.prepare_bbox(face_bbox, face_feat)
        self.body_keypoints = BodyKeypoints.prepare_body_keypoints(body_keypoints)
        self.left_eye_feats = EyeFeatures.prepare_eye_features(left_eye_feats)
        self.right_eye_feats = EyeFeatures.prepare_eye_features(right_eye_feats)

    def set_face(self, bbox=None, feat=None):
        if bbox is not None:
            self.face = FaceBBox.prepare_bbox(bbox, feat)
        if feat is not None and bbox is None:
            self.face.feat = feat

    def set_body_keypoints(self, body_keypoints=None):
        if body_keypoints is not None:
            self.body_keypoints = BodyKeypoints.prepare_body_keypoints(body_keypoints)

    def set_eye_features(self, left_eye=None, right_eye=None):
        if left_eye is not None:
            self.left_eye_feats = EyeFeatures.prepare_eye_features(left_eye)
        if right_eye is not None:
            self.right_eye_feats = EyeFeatures.prepare_eye_features(right_eye)
