import array
import cv2
import numpy as np
cimport cython
cimport numpy as np
from cpython cimport array

DTYPE = np.float32
ctypedef np.float32_t DTYPE_t

@cython.boundscheck(False)
@cython.wraparound(False)
def parse_keypoints(np.ndarray heat_map):
    heat_map = np.transpose(heat_map, (1, 2, 0))
    heat_map = cv2.resize(heat_map, (0, 0), None, 2, 2)
    heat_map = heat_map[:, :, :-1]
    M = np.abs(np.max(heat_map))
    if M > 1:
        heat_map /= M

    cdef np.ndarray[DTYPE_t, ndim=3] map_left = np.zeros_like(heat_map, dtype=DTYPE)
    cdef np.ndarray[DTYPE_t, ndim=3] map_right = np.zeros_like(heat_map, dtype=DTYPE)
    cdef np.ndarray[DTYPE_t, ndim=3] map_up = np.zeros_like(heat_map, dtype=DTYPE)
    cdef np.ndarray[DTYPE_t, ndim=3] map_down = np.zeros_like(heat_map, dtype=DTYPE)
    map_left[1:, :, :] = heat_map[:-1, :, :]
    map_right[:-1, :, :] = heat_map[1:, :, :]
    map_up[:, 1:, :] = heat_map[:, :-1, :]
    map_down[:, :-1, :] = heat_map[:, 1:, :]

    cdef np.ndarray peaks_binary = np.logical_and.reduce(
        (heat_map >= map_left, heat_map >= map_right, heat_map >= map_up, heat_map >= map_down, heat_map > 0.15))

    cdef tuple nz = np.nonzero(peaks_binary)
    cdef list all_peaks = list()
    for _ in range(heat_map.shape[2]):
        all_peaks.append(list())
    cdef int part
    for n in range(len(nz[2])):
        part = nz[2][n]
        all_peaks[part].append(tuple((nz[1][n] * 2, nz[0][n] * 2, heat_map[nz[0][n], nz[1][n], part], n)))

    # all_peaks = map(lambda peaks: sorted(peaks, key=lambda k: k[0]), all_peaks)

    return all_peaks