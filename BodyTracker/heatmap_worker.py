"""
NeurodataLab LLC 24.01.2018
Created by Andrey Belyaev
"""
import cv2
import numpy as np
from cython.parse_keypoints import parse_keypoints

limb_seq = [[2, 3], [2, 6], [3, 4], [4, 5], [6, 7], [7, 8], [2, 9], [9, 10],
            [10, 11], [2, 12], [12, 13], [13, 14], [2, 1], [1, 15], [15, 17],
            [1, 16], [16, 18], [3, 17], [6, 18]]

map_idx = [[31, 32], [39, 40], [33, 34], [35, 36], [41, 42], [43, 44], [19, 20], [21, 22],
           [23, 24], [25, 26], [27, 28], [29, 30], [47, 48], [49, 50], [53, 54], [51, 52],
           [55, 56], [37, 38], [45, 46]]

mid_num = 11
threshold = 0.05


def separate_body_parts(heat_map, limb_map):
    keypoints = parse_keypoints(heat_map)
    limb_map = cv2.resize(limb_map.transpose((1, 2, 0)), None, fx=4, fy=4)
    img_shape = limb_map.shape

    connections_all, special_k, special_non_zero_index = [], [], []

    for k in range(len(map_idx)):
        score_mid = limb_map[:, :, [x - 19 for x in map_idx[k]]]
        candidate1, candidate2 = keypoints[limb_seq[k][0] - 1], keypoints[limb_seq[k][1] - 1]
        n1, n2 = len(candidate1), len(candidate2)
        index1, index2 = limb_seq[k]

        if n1 != 0 and n2 != 0:
            connection_candidates = []
            for i1, c1 in enumerate(candidate1):
                for i2, c2 in enumerate(candidate2):
                    try:
                        vec = np.array(c2)[:2] - np.array(c1)[:2]
                        norm = np.sqrt((vec ** 2).sum())
                        if abs(norm) < 1e-7:
                            continue
                        vec /= norm

                        start_end = zip(np.linspace(c1[0], c2[0], num=mid_num), np.linspace(c1[1], c2[1], num=mid_num))
                        vec_x = np.array([score_mid[int(start_end[i][1] + 1), int(start_end[i][0] + 1), 0]
                                          for i in range(len(start_end))])
                        vec_y = np.array([score_mid[int(start_end[i][1] + 1), int(start_end[i][0] + 1), 1]
                                          for i in range(len(start_end))])

                        score_mid_points = vec_x * vec[0] + vec_y * vec[1]
                        score_with_dist_prior = sum(score_mid_points) / len(score_mid_points) + \
                                                min(0.5 * img_shape[0] / norm - 1, 0)

                        criterion1 = len(np.nonzero(score_mid_points > threshold)[0]) > 0.8 * len(score_mid_points)
                        criterion2 = score_with_dist_prior > 0

                        if criterion1 and criterion2:
                            connection_candidates.append(
                                [i1, i2, score_with_dist_prior, score_with_dist_prior + c1[2] + c2[2]])
                    except:
                        pass

            connection_candidates = sorted(connection_candidates, key=lambda cc: cc[2], reverse=True)
            connection = np.zeros((0, 5))
            for candidate in connection_candidates:
                i, j, s = candidate[:3]
                if i not in connection[:, 3] and j not in connection[:, 4]:
                    connection = np.vstack((connection, [candidate1[i][3], candidate2[j][3], s, i, j]))
                    if len(connection) >= min(n1, n2):
                        break

            connections_all.append(connection)
        else:
            special_k.append(k)
            special_non_zero_index.append(index1 if n1 != 0 else index2)
            connections_all.append([])

    subset = -1 * np.ones((0, 20))
    candidates = np.array([item for sublist in keypoints for item in sublist])

    for k in range(len(map_idx)):
        if k not in special_k:
            try:
                parts1, parts2 = connections_all[k][:, 0], connections_all[k][:, 1]
                index1, index2 = np.array(limb_seq[k]) - 1

                for i in range(len(connections_all[k])):
                    found = 0
                    subset_idx = [-1, -1]
                    for j in range(len(subset)):
                        if subset[j][index1] == parts1[i] or subset[j][index2] == parts2[i]:
                            subset_idx[found] = j
                            found += 1

                    if found == 1:
                        j = subset_idx[0]
                        if subset[j][index2] != parts2[i]:
                            subset[j][index2] = parts2[i]
                            subset[j][-1] += 1
                            subset[j][-2] += candidates[parts2[i].astype(int), 2] + connections_all[k][i][2]
                    elif found == 2:
                        j1, j2 = subset_idx
                        membership = ((subset[j1] >= 0).astype(int) + (subset[j2] >= 0).astype(int))[:-2]
                        if len(np.nonzero(membership == 2)[0]) == 0:
                            subset[j1][:-2] += subset[j2][:-2] + 1
                            subset[j1][-2:] += subset[j2][-2:]
                            subset[j1][-2] += connections_all[k][i][2]
                            subset = np.delete(subset, j2, 0)
                        else:
                            subset[j][index2] = parts2[i]
                            subset[j][-1] += 1
                            subset[j][-2] += candidates[parts2[i].astype(int), 2] + connections_all[k][i][2]
                    elif not found and k < 17:
                        row = -1 * np.ones(20)
                        row[index1] = parts1[i]
                        row[index2] = parts2[i]
                        row[-1] = 2
                        row[-2] = sum(candidates[connections_all[k][i, :-2].astype(int), 2]) + connections_all[k][i][2]
                        subset = np.vstack([subset, row])
            except:
                pass

    delete_idx = [i for i in range(len(subset)) if subset[i][-1] < 4 or subset[i][-2] / subset[i][-1] < 0.4]

    subset = np.delete(subset, delete_idx, axis=0)
    candidates = sorted(candidates, key=lambda c: c[-1])
    return subset, candidates


def create_skeletons(subset, candidates):
    skeletons = [[None for _ in range(18)] for __ in subset]
    for n_set, point_set in enumerate(subset):
        for n_keypoint, kp in enumerate(point_set[:17]):
            if int(kp) != -1:
                skeletons[n_set][n_keypoint] = candidates[int(kp)]
    return skeletons


def process_body_tracker_results(heat_map, limb_map):
    subset, candidates = separate_body_parts(heat_map, limb_map)
    skeletons = create_skeletons(subset, candidates)
    return skeletons


# if __name__ == '__main__':
#     import os
#     import os.path as osp
#     body_tracker_keypoints_path = '/home/mopkobka/NDL-Projects/autonomous_fulltracker/_TMP_DIR/_Colbert524/tmp_bodytracker.npyALL.npy'
#     images_dir = '/home/mopkobka/NDL-Projects/autonomous_fulltracker/_TMP_DIR/_Colbert524/images'
#
#     body_tracker_info = np.load(body_tracker_keypoints_path).tolist()
#     images_list = sorted(os.listdir(images_dir))
#     for img_path in images_list[50:]:
#         image = cv2.imread(osp.join(images_dir, img_path))
#         cur_info = body_tracker_info[img_path]
#         keypoints = cur_info['parsed']
#         maps = cur_info['clear']
#
#         S, C = separate_body_parts(maps[0], maps[1], image.shape)
#
#         people_skeletons = create_skeletons(S, C)
#
#         for n, skeleton in enumerate(people_skeletons):
#             for keypoint in skeleton:
#                 if keypoint is not None:
#                     cv2.circle(image, tuple(map(int, keypoint[:2])), 3, (255, 0, 0) if n == 0 else (0, 0, 255), -1)
#
#         cv2.imshow('image', image)
#         if cv2.waitKey(0) & 0xff == ord('q'):
#             break
