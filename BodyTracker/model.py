"""
NeurodataLab LLC 20.11.2017
Created by Andrey Belyaev

BodyTracker Model
"""
import cv2
import mxnet as mx
import numpy as np
import os
import os.path as osp
from heatmap_worker import process_body_tracker_results
from time import time, sleep
from tqdm import tqdm


Conv = mx.sym.Convolution
Activation = mx.sym.Activation
Concat = mx.sym.concat
Reshape = mx.sym.Reshape


# BodyTracker
# Usage:
#  model = BodyTrackerModel(args..)
#  model.predict_on_folder(data_path, out_name)  <- data_path: data to predict, out_name: path to save keypoints
class BodyTrackerModel:
    def __init__(self, model_path, width, height, ctx, batch_size=8):
        self.model_path = model_path
        sym, arg_params, aux_params = self._create_openpose_model()
        self.target_size = (width, height)[::-1]
        self.data_names = ['data']
        self.data_shapes = [(batch_size, 3, height, width)]
        self.batch_size = batch_size
        if isinstance(ctx, int):
            self.ctx = mx.gpu(ctx)
        elif isinstance(ctx, (tuple, list)):
            self.ctx = [mx.gpu(c) for c in ctx]
        else:
            self.ctx = ctx
        self.model = mx.module.Module(symbol=sym, data_names=self.data_names, label_names=None, context=self.ctx)
        self.model.bind(data_shapes=zip(self.data_names, self.data_shapes), for_training=False)
        self.model.init_params(arg_params=arg_params, aux_params=aux_params)

        # zero forward
        self.model.forward(mx.io.DataBatch(data=[mx.nd.zeros(self.data_shapes[0])]))

    def _create_openpose_model(self, stages_count=4):
        def stage1(inp):
            global_params = [
                [(3, 3), 128, (1, 1)],
                [(3, 3), 128, (1, 1)],
                [(3, 3), 128, (1, 1)],
                [(1, 1), 512, (0, 0)]
            ]

            prev_l1, prev_l2 = inp, inp
            for n, (kernel_size, num_filter, pad) in enumerate(global_params):
                prev_l1 = Conv(data=prev_l1, kernel=kernel_size, num_filter=num_filter, pad=pad,
                               name='conv5_%d_CPM_L1' % n)
                prev_l1 = Activation(data=prev_l1, act_type='relu', name='relu5_%d_CPM_L1' % n)
                prev_l2 = Conv(data=prev_l2, kernel=kernel_size, num_filter=num_filter, pad=pad,
                               name='conv5_%d_CPM_L2' % n)
                prev_l2 = Activation(data=prev_l2, act_type='relu', name='relu5_%d_CPM_L2' % n)

            l1 = Conv(data=prev_l1, kernel=(1, 1), num_filter=38, name='conv5_5_CPM_L1')
            # l1 = Activation(data=l1, act_type='relu')
            l2 = Conv(data=prev_l2, kernel=(1, 1), num_filter=19, name='conv5_5_CPM_L2')
            l2 = Activation(data=l2, act_type='relu')
            return l1, l2

        def stageN(inp, stage_num):
            global_params = [
                [(7, 7), 128, (3, 3)],
                [(7, 7), 128, (3, 3)],
                [(7, 7), 128, (3, 3)],
                [(7, 7), 128, (3, 3)],
                [(7, 7), 128, (3, 3)],
                [(1, 1), 128, (0, 0)]
            ]

            prev_l1, prev_l2 = inp, inp
            for n, (kernel_size, num_filter, pad) in enumerate(global_params):
                prev_l1 = Conv(data=prev_l1, kernel=kernel_size, num_filter=num_filter, pad=pad,
                               name='Mconv%d_stage%d_L1' % (n, stage_num))
                prev_l1 = Activation(data=prev_l1, act_type='relu', name='Mrelu%d_stage%d_L1' % (n, stage_num))
                prev_l2 = Conv(data=prev_l2, kernel=kernel_size, num_filter=num_filter, pad=pad,
                               name='Mconv%d_stage%d_L2' % (n, stage_num))
                prev_l2 = Activation(data=prev_l2, act_type='relu', name='Mrelu%d_stage%d_L2' % (n, stage_num))

            l1 = Conv(data=prev_l1, kernel=(1, 1), num_filter=38, name='Mconv7_stage%d_L1' % stage_num)
            l2 = Conv(data=prev_l2, kernel=(1, 1), num_filter=19, name='Mconv7_stage%d_L2' % stage_num)
            l2 = Activation(data=l2, act_type='relu')
            return l1, l2

        sym, arg_params, aux_params = mx.model.load_checkpoint(self.model_path, 0)
        # sym, arg_params, aux_params = mx.model.load_checkpoint('/home/mopkobka/NDL-Projects/mxnetprojects/BodyTracker/mxNetsLogs/body_tracker_fine_tune', 855)
        feat = sym.get_internals()['relu3_4_output']

        heat_map = mx.symbol.Variable('heat_map')
        heat_map = Reshape(data=heat_map, shape=(-1,))
        limb_map = mx.symbol.Variable('limb_map')
        limb_map = Reshape(data=limb_map, shape=(-1,))
        heat_weights = mx.symbol.Variable('heat_map_weights')
        heat_weights = Reshape(data=heat_weights, shape=(-1,))
        limb_weights = mx.symbol.Variable('limb_map_weights')
        limb_weights = Reshape(data=limb_weights, shape=(-1,))

        out_layers = []

        # Stage 1
        stage1_l1, stage1_l2 = stage1(feat)
        for layer, label, weights in zip((stage1_l2, stage1_l1), (heat_map, limb_map), (heat_weights, limb_weights)):
            out_layers.append(layer)

        # Stage 2-6
        prev_l1, prev_l2 = stage1_l1, stage1_l2
        for stage in range(2, stages_count + 1):
            c = Concat(prev_l1, prev_l2, feat, dim=1, name='concat_stage%d' % stage)
            prev_l1, prev_l2 = stageN(c, stage)
            for layer, label, weights in zip((prev_l2, prev_l1), (heat_map, limb_map), (heat_weights, limb_weights)):
                out_layers.append(layer)

        grouped = mx.sym.Group(out_layers[-2:])

        return grouped, arg_params, aux_params

    def predict_on_folder(self, data_path, out_name, human_position):
        images, paths = [], []
        info = {}
        # info = AsyncDict(parse_keypoints)
        for im_path in os.listdir(data_path):
            img = cv2.imread(osp.join(data_path, im_path))
            img = cv2.resize(img, self.target_size[::-1])
            if img.max() > 1:
                img = img.astype(float) / 256. - 0.5
            img = img.transpose((2, 0, 1))
            images.append(img)
            paths.append(im_path)

            if len(images) == self.batch_size:
                self.update_info(info, images, paths, human_position, data_path)
                del images, paths
                images, paths = [], []

        if len(images) > 0:
            for _ in range(len(images), self.batch_size):
                images.append(np.zeros(self.data_shapes[0][1:]))
                paths.append(None)
            self.update_info(info, images, paths, human_position)

        np.save(out_name, info)

    def update_info(self, info, images, paths, human_position, data_path=None):
        self.model.forward(mx.io.DataBatch(data=[mx.nd.array(images)]), is_train=False)
        predictions = self.model.get_outputs()

        heat_maps = predictions[0].asnumpy()
        limb_maps = predictions[1].asnumpy()

        for im_path, heat_map, limb_map in zip(paths, heat_maps, limb_maps):
            if im_path is not None:
                info[im_path] = process_body_tracker_results(heat_map, limb_map)

if __name__ == '__main__':
    from easydict import EasyDict as edict
    import yaml
    bt_config = edict(yaml.load(open('/home/mopkobka/NDL-Projects/autonomous_fulltracker/config.yml'))).BODYTRACKER
    bt_config.BATCH_SIZE = 32
    bt_config.GPU = 1

    model = BodyTrackerModel(bt_config.NET, bt_config.WIDTH, bt_config.HEIGHT, bt_config.GPU, bt_config.BATCH_SIZE)
    start = time()
    model.predict_on_folder('/home/mopkobka/NDL-Projects/autonomous_fulltracker/_TMP_DIR/_Colbert524/images',
                            '/home/mopkobka/NDL-Projects/autonomous_fulltracker/_TMP_DIR/_Colbert524/tmp_bodytracker.npy',
                            'left')
    print time() - start
