"""
NeurodataLab LLC 24.01.2018
Created by Andrey Belyaev
"""

import argparse
import json
import numpy as np
import os
import os.path as osp
import pika
from receivers.rabbitmq_config import *
from subprocess import call
from time import sleep
from tracker_utils.config import *


def check_videos(all_videos, in_path):
    videos_list = []
    for fold in os.listdir(in_path):
        for p in os.listdir(osp.join(in_path, fold)):
            videos_list.append(osp.join(fold, p))
    good_videos, bad_videos = [], []
    for video_info in all_videos:
        video_path = video_info['video_path']
        if video_path in videos_list:
            good_videos.append(video_path)
        else:
            bad_videos.append(video_path)
    return good_videos, bad_videos


def create_video_list(all_videos, out_path):
    processed_videos = os.listdir(out_path)
    new_v, old_v = [], []
    for video_info in all_videos:
        video_name = video_info['video_name']
        if video_name in processed_videos:
            old_v.append(video_info)
        else:
            new_v.append(video_info)
    return new_v, old_v


def create_queues(channel, *queues):
    for queue in queues:
        channel.queue_declare(queue=queue)


if __name__ == '__main__':
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', heartbeat=0))
    channel = connection.channel()
    # create_queues(channel, *QUEUES.values())
    publisher = GlobalPublisher(channel)

    with open(CONF.GLOBAL.VIDEOS_INFO, 'r') as f:
        videos_info = json.load(f)

    # gv, bv = check_videos(videos_info, CONF.GLOBAL.VIDEO_PATHS)
    new_videos, old_videos = create_video_list(videos_info, CONF.GLOBAL.OUT_DIR)

    idxs = np.random.permutation(len(new_videos))
    new_videos = [new_videos[i] for i in idxs]

    # publisher.voice_separator.publish(video_name='aolbuild41', audio_path='/SSD/_TMP_DIR/aolbuild41/aolbuild41.wav')

    # exit(0)
    # publisher.final_processing.publish(video_name='_Colbert529')

    # new_videos = list(map(lambda p: '_10_Questions/_10_Questions%d.mp4' % p, (9, 1, 40, 48)))

    for n, video in enumerate(new_videos):
        # if 'Ellen10.' not in video['video_path']:
        #     continue
        # if 'jobint316' not in video['video_path'] and 'Colbert529' not in video['video_path']:
        #    continue
        publisher.image_parser.publish(video_path=osp.join(CONF.GLOBAL.VIDEO_PATHS, video['video_path']))
        log_info(channel, 'Add to queue: %s' % video['video_path'])
        # call(['python2', 'tracker.py', '-v', osp.join(CONF.GLOBAL.VIDEO_PATHS, video['video_path'])])
        # call(['python2', 'tracker.py', '-v', osp.join(CONF.GLOBAL.VIDEO_PATHS, video)])
        # break
        # sleep(180)
        exit(0)

    # connection.close()
