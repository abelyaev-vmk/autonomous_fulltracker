#!/usr/bin/env bash

ps afx | grep -v grep| grep "python2 receivers/timer_receiver.py" | awk '{print $1}' | xargs kill -9
ps afx | grep -v grep| grep "python2 receivers/logs_receiver.py" | awk '{print $1}' | xargs kill -9
ps afx | grep -v grep| grep "python2 receivers/final_processing.py" | awk '{print $1}' | xargs kill -9
ps afx | grep -v grep| grep "python2 receivers/image_parser_receiver.py" | awk '{print $1}' | xargs kill -9
ps afx | grep -v grep| grep "python2 receivers/body_tracker_receiver.py" | awk '{print $1}' | xargs kill -9
ps afx | grep -v grep| grep "python3 receivers/face_detector_receiver.py" | awk '{print $1}' | xargs kill -9
ps afx | grep -v grep| grep "python2 receivers/scene_change_detector_receiver.py" | awk '{print $1}' | xargs kill -9
ps afx | grep -v grep| grep "python2 receivers/face_tracker_receiver.py" | awk '{print $1}' | xargs kill -9
ps afx | grep -v grep| grep "python2 receivers/eye_tracker_receiver.py" | awk '{print $1}' | xargs kill -9
ps afx | grep -v grep| grep "python2 receivers/voice_separator_receiver.py" | awk '{print $1}' | xargs kill -9

echo "All receivers killed"
