"""
NeurodataLab LLC 20.11.2017
Created by Andrey Belyaev

Generate config file from existing config.py
"""
import argparse
import yaml
from config import *


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('--out-file', type=str, default='config.yml')
    return parser.parse_args()


def make_dict(edict):
    if isinstance(edict, EasyDict):
        return {key: make_dict(val) for key, val in edict.items()}
    else:
        return edict


if __name__ == '__main__':
    arguments = parse()
    with open(arguments.out_file, 'w') as f:
        yaml.dump(make_dict(CONF), f, default_flow_style=False)
