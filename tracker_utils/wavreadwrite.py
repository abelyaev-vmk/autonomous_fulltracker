"""
NeurodataLab LLC 19.01.2018
Created by Andrey Belyaev
"""
import numpy as np


def read_wave(filename):
    from wave import open as open_wave
    fp = open_wave(filename, 'r')
    n_channels = fp.getnchannels()
    n_frames = fp.getnframes()
    sample_width = fp.getsampwidth()
    fs = fp.getframerate()
    z_str = fp.readframes(n_frames)
    fp.close()
    dtype_map = {1: np.int8, 2: np.int16, 3: 'special', 4: np.int32}
    if sample_width not in dtype_map:
        raise ValueError('sample_width %d unknown' % sample_width)
    if sample_width == 3:
        xs = np.fromstring(z_str, dtype=np.int8).astype(np.int32)
        ys = (xs[2::3] * 256 + xs[1::3]) * 256 + xs[0::3]
    else:
        ys = np.fromstring(z_str, dtype=dtype_map[sample_width])
    # if it's in stereo, just pull out the first channel
    if n_channels == 2:
        ys = ys[::2]
    return ys / np.max(np.abs(ys)), fs


def write_wave(filename, x, fs):
    from wave import open as open_wave
    n_channels = 1
    sample_width = 2
    bits = sample_width * 8
    bound = 2 ** (bits - 1) - 1
    max_val = np.max(abs(x))
    if max_val > 1:
        x = x / (max_val + 1e-5)
    x = (x * bound).astype(np.int16)
    fp = open_wave(filename, 'w')
    fp.setnchannels(n_channels)
    fp.setsampwidth(sample_width)
    fp.setframerate(fs)
    fp.writeframes(x.tostring())
    fp.close()
