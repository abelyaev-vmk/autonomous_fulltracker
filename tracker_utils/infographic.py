"""
NeurodataLab LLC 09.02.2018
Created by Andrey Belyaev
"""

import pickle
import numpy as np
import os
import os.path as osp
from matplotlib import pyplot as plt
from tqdm import tqdm


def create_video_info(results_path):
    va_path = osp.join(results_path, 'VideoAnalyzer.pckl')
    if not osp.exists(va_path):
        return None

    with open(va_path, 'r') as f:
        va = pickle.load(f)

    main_person = va.persons.main
    if main_person is None:
        return None

    ret = {frame: None for frame in va.all_frames}

    for frame in va.all_frames:
        ret[frame] = {
            'body': 1 if frame in main_person.bodies and main_person.bodies[frame] is not None else 0,
            'face': 1 if frame in main_person.face_bboxes and main_person.face_bboxes[frame] is not None else 0,
            'left_eye': 1 if frame in main_person.left_eyes and main_person.left_eyes[frame].eye is not None else 0,
            'right_eye': 1 if frame in main_person.right_eyes and main_person.right_eyes[frame].eye is not None else 0
        }

    return ret


if __name__ == '__main__':
    data_path = '/RAID/FullTracker_Results'
    all_videos_info = {}
    for video_name in os.listdir(data_path):
        video_info = create_video_info(osp.join(data_path, video_name))
        if video_info is None:
            continue
        else:

            frames = np.array(sorted(video_info.keys()))
            body = np.array([video_info[frame]['body'] for frame in frames])  # green
            face = np.array([video_info[frame]['face'] * 2 for frame in frames])  # blue
            left_eye = np.array([video_info[frame]['left_eye'] * 3 for frame in frames])  # magenta
            right_eye = np.array([video_info[frame]['right_eye'] * 4 for frame in frames])  # red

            fig = plt.figure()

            # draw frames
            plt.plot(frames, [0] * len(frames), marker='o', color='yellow', markersize=2)

            # draw right eyes
            gf = [frame for frame in range(len(frames)) if right_eye[frame] > 0]
            gp = right_eye[gf].tolist()
            plt.bar(frames[gf], gp, width=1, color='red')
            plt.bar(frames[gf], [3] * len(gp), width=1, color='white')

            # draw left eyes
            gf = [frame for frame in range(len(frames)) if left_eye[frame] > 0]
            gp = left_eye[gf].tolist()
            plt.bar(frames[gf], gp, width=1, color='magenta')
            plt.bar(frames[gf], [2] * len(gp), width=1, color='white')

            # draw faces
            gf = [frame for frame in range(len(frames)) if face[frame] > 0]
            gp = face[gf].tolist()
            plt.bar(frames[gf], gp, width=1, color='blue')
            plt.bar(frames[gf], [1] * len(gp), width=1, color='white')

            # draw body
            gf = [frame for frame in range(len(frames)) if body[frame] > 0]
            gp = body[gf].tolist()
            plt.bar(frames[gf], gp, width=1, color='green')

            good_body = len(np.where(body > 0)[0])
            good_face = len(np.where(face > 0)[0])
            good_left_eye = len(np.where(left_eye > 0)[0])
            good_right_eye = len(np.where(right_eye > 0)[0])
            good_body_only = len(np.where(np.logical_and(body > 0, face == 0))[0])
            good_face_only = len(np.where(np.logical_and(body == 0, face > 0))[0])
            good_body_face = len(np.where(np.logical_and(body > 0, face > 0))[0])

            all_videos_info[video_name] = [len(frames), good_body, good_face, good_left_eye, good_right_eye,
                                           good_body_only, good_face_only, good_body_face]
            info_str = ' body %.2f  face %.2f  left_eye %.2f  right_eye %.2f' % \
                       tuple(map(lambda a: float(a) / len(frames),
                                 (good_body, good_face, good_left_eye, good_right_eye)))

            print '%s: %s' % (video_name, info_str)

            fig.suptitle(info_str, fontsize=14, fontweight='bold')

            plt.savefig(osp.join(data_path, video_name, 'infographic.pdf'), format='pdf')
            plt.clf()
            # plt.show()

        # if len(all_videos_info) > 0:
        #     break

    print '\n'
    print 'Total video', len(all_videos_info)
    total_frames = sum(map(lambda i: i[0], all_videos_info.values()))
    print 'Total frames', total_frames
    print 'Good body', float(sum(map(lambda i: i[1], all_videos_info.values()))) / total_frames
    print 'Good face', float(sum(map(lambda i: i[2], all_videos_info.values()))) / total_frames
    print 'Good left_eye', float(sum(map(lambda i: i[3], all_videos_info.values()))) / total_frames
    print 'Good right_eye', float(sum(map(lambda i: i[4], all_videos_info.values()))) / total_frames

    print 'Good body & bad face', float(sum(map(lambda i: i[5], all_videos_info.values()))) / total_frames
    print 'Bad body & good face', float(sum(map(lambda i: i[6], all_videos_info.values()))) / total_frames
    print 'Good body & good face', float(sum(map(lambda i: i[7], all_videos_info.values()))) / total_frames
