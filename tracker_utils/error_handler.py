"""
NeurodataLab LLC 11.01.2018
Created by Andrey Belyaev
"""
import json
import sys
from receivers.rabbitmq_config import log_warn

python_version = sys.version_info.major


def handle_receiver_errors(channel):
    def handle_receiver_errors_wrapper(foo):
        def handler(*args, **kwargs):
            try:
                body = kwargs['body'] if 'body' in kwargs else args[-1]
                body = body if python_version == 2 else body.decode('utf-8')
                body = json.loads(body)
            except:
                body = {'video_name': 'unknown'}

            try:
                error_str = None
                res = foo(*args, **kwargs)
            except NotImplementedError as e:
                error_str = 'Troubles with ' + foo.__name__ + ': %s' % str(e)
                res = None
            except:
                error_str = 'Troubles with ' + foo.__name__
                if 'video_name' in body:
                    error_str += ' with video ' + body['video_name']
                elif 'video_path' in body:
                    error_str += ' with video' + '.'.join(body['video_path'].split('/')[-1].split('.')[:-1])
                res = None
            finally:
                if error_str is not None:
                    if channel is not None:
                        log_warn(channel, error_str)
                    else:
                        print(error_str)

            return res

        return handler

    return handle_receiver_errors_wrapper
