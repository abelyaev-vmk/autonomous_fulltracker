"""
NeurodataLab LLC 26.01.2018
Created by Andrey Belyaev
"""
import cv2
import json
import math
import numpy as np
import os
import os.path as osp
import pickle
from matplotlib import pyplot as plt
from wavreadwrite import read_wave


def visualize_results(images_dir, marking_path, out_path, key_corresponding_path):
    with open(marking_path, 'r') as f:
        marking = pickle.load(f)
        marking = {int(k): v for k, v in marking.items()}
    with open(key_corresponding_path, 'r') as f:
        key_corr = json.load(f)
        key_corr = {int(k): v for k, v in key_corr.items()}

    if not osp.exists(out_path):
        os.mkdir(out_path)

    marking_keys = sorted(marking.keys())

    for n, key in enumerate(marking_keys):
        if key not in key_corr:
            continue
        value = marking[key]

        img = cv2.imread(osp.join(images_dir, key_corr[key]))
        img = visualize_faces(img, value['face'], with_id=True, with_speak=True)
        img = visualize_body(img, value['body'])

        # cv2.imshow('image', img)
        # if cv2.waitKey(0) & 0xff == ord('q'):
        #     exit(0)
        cv2.imwrite(osp.join(out_path, '%06d.jpg' % n), img)


def visualize_faces(img, faces, with_id=True, with_speak=True):
    colors = [(0, 0, 255), (0, 255, 0), (255, 0, 0)]
    for n, face in faces:
        color = colors[n % len(colors)]
        f_x, f_y, f_w, f_h = map(int, (face['x'], face['y'], face['w'], face['h']))
        cv2.rectangle(img, (f_x, f_y), (f_x + f_w, f_y + f_h), color, 2)
        if with_speak:
            cv2.putText(img, str(face['speak'][1]), (f_x, f_y - 5), cv2.FONT_HERSHEY_COMPLEX, 0.4, color)
        if with_id:
            cv2.putText(img, 'id_%d' % n, (f_x, f_y + f_h + 12), cv2.FONT_HERSHEY_COMPLEX, 0.4, color)

    return img


def visualize_body(img, skeletons):
    body_image = img.copy()
    colors = [[255, 0, 0], [255, 85, 0], [255, 170, 0], [255, 255, 0], [170, 255, 0], [85, 255, 0], [0, 255, 0],
              [0, 255, 85], [0, 255, 170], [0, 255, 255], [0, 170, 255], [0, 85, 255], [0, 0, 255], [85, 0, 255],
              [170, 0, 255], [255, 0, 255], [255, 0, 170], [255, 0, 85]]

    limb_seq = [[2, 3], [2, 6], [3, 4], [4, 5], [6, 7], [7, 8], [2, 9], [9, 10],
                [10, 11], [2, 12], [12, 13], [13, 14], [2, 1], [1, 15], [15, 17],
                [1, 16], [16, 18], [3, 17], [6, 18]]

    for skeleton in skeletons:
        for i in range(17):
            try:
                idx1, idx2 = np.array(limb_seq[i]) - 1
                point1, point2 = skeleton[idx1], skeleton[idx2]
                if point1 is None or point2 is None:
                    continue
                point1, point2 = np.array(point1), np.array(point2)
                m_x = np.mean((point1[0], point2[0]))
                m_y = np.mean((point1[1], point2[1]))
                length = ((point1 - point2) ** 2).sum() ** 0.5
                angle = math.degrees(math.atan2(point1[1] - point2[1], point1[0] - point2[0]))
                polygon = cv2.ellipse2Poly((int(m_x), int(m_y)), (int(length / 2), 4), int(angle), 0, 360, 1)
                cv2.fillConvexPoly(body_image, polygon, colors[i])
            except:
                pass
    return cv2.addWeighted(img, 0.5, body_image, 0.5, 0)


def eye_audio_plot(marking_path, key_corresponding_path, parsed_data_path):
    with open(marking_path, 'r') as f:
        marking = pickle.load(f)
        marking = {int(k): v for k, v in marking.items()}
    with open(key_corresponding_path, 'r') as f:
        key_corr = json.load(f)
        key_corr = {int(k): v for k, v in key_corr.items()}

    marking_keys = sorted(marking.keys())

    audio1, audio2, lips1, lips2 = [], [], {}, {}

    for n, key in enumerate(marking_keys):
        if key not in key_corr:
            continue
        value = marking[key]
        sec = value['sec']
        faces = value['face']
        if len(faces) == 1:
            n, face = faces[0]
            lips1[sec] = face['speak'][1] if n == 0 else 0
            lips2[sec] = face['speak'][1] if n == 1 else 0
        else:
            for n, face in faces[:2]:
                if n == 0:
                    lips1[sec] = face['speak'][1]
                else:
                    lips2[sec] = face['speak'][1]
    lips1_x = sorted(map(float, lips1.keys()))
    lips1_y = map(lambda k: float(lips1[k]) + 2, lips1_x)
    lips2_x = sorted(map(float, lips2.keys()))
    lips2_y = map(lambda k: float(lips2[k]) + 3, lips2_x)

    audio_paths = filter(lambda p: 'full' not in p and p.endswith('wav'), os.listdir(parsed_data_path))
    audio_pairs = []
    cur_part = 0
    while True:
        audio_part = sorted(filter(lambda p: ('part%d' % cur_part) in p, audio_paths))
        if len(audio_part) == 0:
            break
        audio_pairs.append((read_wave(osp.join(parsed_data_path, audio_part[0]))[0].tolist(),
                            read_wave(osp.join(parsed_data_path, audio_part[1]))[0].tolist()))
        cur_part += 1

    audio1_y, audio2_y = [], []
    for audio1, audio2 in audio_pairs:
        audio1_y.extend(audio1)
        audio2_y.extend(audio2)
    audio1_y = map(lambda a: 0 if abs(a) < 1e-5 else 1, audio1_y)
    audio2_y = map(lambda a: 1 if abs(a) < 1e-5 else 2, audio2_y)
    audio1_x = [i / 44100. for i in range(len(audio1_y))]
    audio2_x = [i / 44100. for i in range(len(audio2_y))]

    plt.figure()
    plt.plot(lips1_x, lips1_y)
    plt.plot(lips2_x, lips2_y)
    plt.plot(audio1_x, audio1_y, linewidth=1)
    plt.plot(audio2_x, audio2_y, linewidth=1)
    plt.savefig(osp.join(parsed_data_path, 'eye_audio_plot.png'))
    plt.close()
    # plt.show()

if __name__ == '__main__':
    # visualize_results('/home/mopkobka/NDL-Projects/autonomous_fulltracker/_TMP_DIR/_Colbert524/images',
    #                   '/RAID/FullTracker_Results/_Colbert524/marking.pckl',
    #                   '/RAID/FullTracker_Results/_Colbert524/visualize',
    #                   '/RAID/FullTracker_Results/_Colbert524/key_corresponding.json')

    eye_audio_plot('/RAID/FullTracker_Results/_Colbert524/marking.pckl',
                   '/RAID/FullTracker_Results/_Colbert524/key_corresponding.json',
                   '/RAID/FullTracker_Results/_Colbert524')

    # visualize_faces('/home/mopkobka/NDL-Projects/autonomous_fulltracker/_TMP_DIR/_Ellen10/images',
    #                 '/RAID/FullTracker_Results/_Ellen10/all_faces.json')
