"""
NeurodataLab LLC 23.11.2017
Created by Andrey Belyaev
"""
import cv2
import json
import mxnet as mx
import numpy as np
import os.path as osp
from pupil_detection import detect_pupil


# EyeClassification
# Classify eye for open\close category
class EyeClassificationModel:
    def __init__(self, model_path, width, height, ctx, batch_size):
        sym, arg_params, aux_params = mx.model.load_checkpoint(model_path, 0)
        self.eye_box_size = np.array([15, 15])
        self.pupil_box_size = np.array([7, 7])
        self.target_size = width, height
        self.data_names = ['data']
        self.data_shapes = [(batch_size, 3, width, height)]
        self.batch_size = batch_size
        if isinstance(ctx, int):
            self.ctx = mx.gpu(ctx)
        elif isinstance(ctx, (tuple, list)):
            self.ctx = [mx.gpu(c) for c in ctx]
        else:
            self.ctx = ctx
        self.model = mx.module.Module(symbol=sym, data_names=self.data_names,
                                      label_names=['open_close_label'],
                                      context=self.ctx)
        self.model.bind(data_shapes=zip(self.data_names, self.data_shapes), for_training=False)
        self.model.init_params(arg_params=arg_params, aux_params=aux_params, allow_missing=True, allow_extra=True)

        # Zero forward
        self.model.forward(mx.io.DataBatch(data=[mx.nd.zeros(self.data_shapes[0])]))

    def predict_batch(self, left_eyes, right_eyes, left_offsets, right_offsets, paths, predictions):
        images = []
        for left_eye, right_eye in zip(left_eyes, right_eyes):
            le = cv2.resize(left_eye, self.target_size).transpose((2, 0, 1))
            re = cv2.resize(right_eye, self.target_size).transpose((2, 0, 1))
            if le.max() > 1:
                le = le.astype(float) / 255.
            if re.max() > 1:
                re = re.astype(float) / 255.
            images += [le, re]
        self.model.forward(mx.io.DataBatch(data=[mx.nd.array(images)]), is_train=False)
        preds = self.model.get_outputs()[0].asnumpy()
        for n, (path, left_eye, right_eye, left_offset, right_offset) in enumerate(zip(paths, left_eyes, right_eyes,
                                                                                       left_offsets, right_offsets)):
            if path is None:
                continue
            le, re = preds[2 * n: 2 * (n + 1)]
            left_pupil = detect_pupil(left_eye, self.eye_box_size, self.pupil_box_size) if le[0] < 0.5 else None
            right_pupil = detect_pupil(right_eye, self.eye_box_size, self.pupil_box_size) if re[0] < 0.5 else None

            right_pupil = (np.array(right_pupil) + np.array(right_offset)).tolist() if right_pupil is not None else None
            left_pupil = (np.array(left_pupil) + np.array(left_offset)).tolist() if left_pupil is not None else None

            predictions[path] = {'left': left_pupil, 'right': right_pupil}

    def predict_on_eyes_centers(self, data_path, person_faces, eyes_centers):
        image_paths = sorted(eyes_centers.keys())
        paths, left_eyes, right_eyes, left_offsets, right_offsets = [], [], [], [], []
        predictions = {}
        for image_path in image_paths:
            image = cv2.imread(osp.join(data_path, image_path))
            bbox = person_faces[image_path]
            x, y, w, h = map(int, (bbox['x'], bbox['y'], bbox['w'], bbox['h']))
            face_image = image[y: y + h, x: x + w]
            shape = face_image.shape[:2]

            eyes_info = eyes_centers[image_path]
            left_eye, right_eye = eyes_info['left'], eyes_info['right']
            for eye, eye_arr, offsets in zip((left_eye, right_eye),
                                             (left_eyes, right_eyes),
                                             (left_offsets, right_offsets)):
                x_, y_ = np.array(eye) - self.eye_box_size
                x_, y_ = map(lambda p: int(max(0, p)), (x_, y_))
                w_, h_ = 2 * self.eye_box_size
                w_, h_ = min(w_, shape[1] - x_), min(h_, shape[0] - y_)
                eye_img = face_image[y_: y_ + h_, x_: x_ + w_]
                offsets.append([x + x_, y + y_])
                eye_arr.append(eye_img)
            paths.append(image_path)
            if len(left_eyes) == self.batch_size // 2:
                self.predict_batch(left_eyes, right_eyes, left_offsets, right_offsets, paths, predictions)
                paths, left_eyes, right_eyes, left_offsets, right_offsets = [], [], [], [], []

        if len(left_eyes) > 0:
            for _ in range(len(left_eyes), self.batch_size // 2):
                left_eyes.append(np.zeros(self.data_shapes[0][1:]).transpose((1, 2, 0)))
                right_eyes.append(np.zeros(self.data_shapes[0][1:]).transpose((1, 2, 0)))
                paths.append(None)
                left_offsets.append(None)
                right_offsets.append(None)
            self.predict_batch(left_eyes, right_eyes, left_offsets, right_offsets, paths, predictions)

        return predictions


# EyeRegression
# Find left eye, right eye and nose
class EyeRegressionModel:
    def __init__(self, model_path, width, height, ctx, batch_size):
        sym, arg_params, aux_params = mx.model.load_checkpoint(model_path, 0)
        self.target_size = width, height
        self.data_names = ['data']
        self.data_shapes = [(batch_size, 3, width, height)]
        self.batch_size = batch_size
        if isinstance(ctx, int):
            self.ctx = mx.gpu(ctx)
        elif isinstance(ctx, (tuple, list)):
            self.ctx = [mx.gpu(c) for c in ctx]
        else:
            self.ctx = ctx
        self.model = mx.module.Module(symbol=sym, data_names=self.data_names,
                                      label_names=['left_x_label', 'right_x_label', 'nose_x_label',
                                                   'left_y_label', 'right_y_label', 'nose_y_label'],
                                      context=self.ctx)
        self.model.bind(data_shapes=zip(self.data_names, self.data_shapes), for_training=False)
        self.model.init_params(arg_params=arg_params, aux_params=aux_params, allow_missing=True)

        # Zero forward
        self.model.forward(mx.io.DataBatch(data=[mx.nd.zeros(self.data_shapes[0])]), is_train=False)

    def predict_batch(self, images, paths, ratios, predictions):
        self.model.forward(mx.io.DataBatch(data=[mx.nd.array(images)]), is_train=False)
        preds = self.model.get_outputs()
        lx_pred, rx_pred, nx_pred, ly_pred, ry_pred, ny_pred = map(lambda p: p.asnumpy(), preds)
        for path, ratio, lx, rx, ly, ry in zip(paths, ratios, lx_pred, rx_pred, ly_pred, ry_pred):
            if path is None:
                continue
            lx_cen, rx_cen, ly_cen, ry_cen = map(lambda p: np.argmax(p), (lx, rx, ly, ry))
            predictions[path] = {
                'left': (lx_cen * ratio[0], ly_cen * ratio[1]),
                'right': (rx_cen * ratio[0], ry_cen * ratio[1])
            }

    def predict_on_faces(self, data_path, person_faces):
        image_paths = sorted(person_faces.keys())
        images, paths, ratios = [], [], []
        predictions = {}
        for image_path in image_paths:
            image = cv2.imread(osp.join(data_path, image_path))
            bbox = person_faces[image_path]
            x, y, w, h = map(int, (bbox['x'], bbox['y'], bbox['w'], bbox['h']))
            face_image = image[y: y + h, x: x + w]
            shape = face_image.shape[:2]
            face_image = cv2.resize(face_image, self.target_size)
            if face_image.max() > 1:
                face_image = face_image.astype(float) / 256. - 0.5
            face_image = face_image.transpose((2, 0, 1))
            images.append(face_image)
            paths.append(image_path)
            ratios.append([float(shape[1]) / self.target_size[0], float(shape[0]) / self.target_size[1]])
            if len(images) == self.batch_size:
                self.predict_batch(images, paths, ratios, predictions)
                images, paths, ratios = [], [], []

        if len(images) > 0:
            for _ in range(len(images), self.batch_size):
                images.append(np.zeros(self.data_shapes[0][1:]))
                paths.append(None)
                ratios.append(None)
            self.predict_batch(images, paths, ratios, predictions)

        return predictions


# EyeTracker
# Consists of EyeRegressor and EyeClassifier
class EyeTrackerModel:
    def __init__(self, classification_params, regression_params):
        init_keys = ('NET', 'WIDTH', 'HEIGHT', 'GPU', 'BATCH_SIZE')
        self.classifier = EyeClassificationModel(*[classification_params[key] for key in init_keys])
        self.regressor = EyeRegressionModel(*[regression_params[key] for key in init_keys])

    def predict(self, data_path, json_path, out_name):
        with open(json_path, 'r') as f:
            person_faces = json.load(f)

        regressed_eyes = self.regressor.predict_on_faces(data_path, person_faces)
        oc_eyes = self.classifier.predict_on_eyes_centers(data_path, person_faces, regressed_eyes)

        with open(out_name, 'w') as f:
            json.dump(oc_eyes, f, indent=2)


# To debug
if __name__ == '__main__':
    import yaml
    from easydict import EasyDict as edict
    config = edict(yaml.load(open('/home/mopkobka/NDL-Projects/FullTracker_v2/config.yml', 'r')))
    model = EyeTrackerModel(config.EYETRACKER.CLASSIFIER, config.EYETRACKER.REGRESSOR)

    model.predict('/home/mopkobka/NDL-Projects/FullTracker_v2/_TMP_DIR/_Colbert30.14/images',
                  '/home/mopkobka/NDL-Projects/FullTracker_v2/_TMP_DIR/_Colbert30.14/_Colbert30.14_id0.json',
                  '/home/mopkobka/NDL-Projects/FullTracker_v2/_TMP_DIR/_Colbert30.14/_Colbert30.14_eyes_id0.json')