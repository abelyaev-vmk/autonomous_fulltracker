"""
NeurodataLab LLC 23.11.2017
Created by Andrey Belyaev

Pupil detection algorithm (CV)
"""
import cv2
import numpy as np


def adjust_gamma(image, gamma=1.0, start=0.):
    invGamma = 1.0 / gamma
    table = np.array([(max(0., (i - start) / (255.0 - start)) ** invGamma) * 255
                      for i in np.arange(0, 256)]).astype("uint8")
    return cv2.LUT(image, table)


def adjust_hist_gamma(image, gamma=1., pct=.01):
    hist = cv2.calcHist([image], [0], None, [256], [0, 256])
    count, gamma_start_pixel = 0., 0
    min_count = pct * image.shape[0] * image.shape[1]
    for gms in range(0, 256):
        count += hist[gms][0]
        if count >= min_count:
            gamma_start_pixel = gms
            break
    return adjust_gamma(image, gamma=gamma, start=gamma_start_pixel)


def detect_pupil(eye_img, eye_box_size, pupil_box_size):
    x_diff, y_diff = eye_box_size[0] - pupil_box_size[0], eye_box_size[1] - pupil_box_size[1]
    pupil_img = eye_img[x_diff: -x_diff, y_diff: -y_diff]
    gray = cv2.cvtColor(pupil_img, cv2.COLOR_BGR2GRAY)
    gray = adjust_hist_gamma(gray, pct=.01)
    cen, count = np.array((0., 0.)), 0.
    bin = np.zeros(gray.shape)
    for i in range(gray.shape[0]):
        for j in range(gray.shape[1]):
            if gray[i, j] < 10:
                cen[0] += i
                cen[1] += j
                count += 1
                bin[i, j] = 255

    return (list(map(int, cen / count)) if count > 0 else np.array(gray.shape[:2].tolist()) / 2)[::-1]