"""
NeurodataLab LLC 20.11.2017
Created by Andrey Belyaev

Asynchronously add new tasks
"""
import argparse
import pika
from receivers.rabbitmq_config import *
from time import time, sleep


def create_queues(channel, *queues):
    for queue in queues:
        channel.queue_declare(queue=queue)


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--video', type=str, default=None)
    parser.add_argument('-d', '--dir', type=str, default=None)
    return parser.parse_args()


if __name__ == '__main__':
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    create_queues(channel, *QUEUES.values())

    arguments = parse()
    publisher = GlobalPublisher(channel)

    if arguments.video is not None:
        publisher.image_parser.publish(video_path=arguments.video)
        log_info(channel, 'Add to queue: %s' % arguments.video.split('/')[-1])
    elif arguments.dir is not None:
        for video in os.listdir(arguments.dir):
            print os.path.join(arguments.dir, video)
            publisher.image_parser.publish(video_path=os.path.join(arguments.dir, video))
            # sleep(1)
    else:
        print 'Specify path for video or for directory'

    connection.close()
