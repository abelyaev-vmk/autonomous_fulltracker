"""
NeurodataLab LLC 23.11.2017
Created by Andrey Belyaev

Asynchronous time logging block
"""
import argparse
import pika
import yaml
from rabbitmq_config import *
from time import time, localtime
from tracker_utils.error_handler import handle_receiver_errors


start_time = time()


def get_time_info(video_name, video_info):
    info = '===========\n'
    info += '-- Video %s\n' % video_name
    sum_time = 0
    for key, item in video_info.items():
        if key == 'start_time':
            continue
        sum_time += item if key != 'body_tracker' else 0
        info += '-- %s: %fs\n' % (key, item)
    sum_time = max(sum_time, video_info['body_tracker'])
    info += '-- Summary: %fs\n' % sum_time
    info += '~ Total time: %fs\n' % (time() - video_info['start_time'])
    return info


if __name__ == '__main__':
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', heartbeat=0))
    channel = connection.channel()
else:
    channel = None


@handle_receiver_errors(channel=channel)
def timer_callback(ch, method, properties, body):
    connection.process_data_events()
    global start_time
    body = edict(json.loads(body))
    if body.video_name not in video_in_process:
        video_in_process[body.video_name] = {}
    video_in_process[body.video_name][body.receiver_name] = body.time
    if set(video_in_process[body.video_name].keys()) == steps_set:
        # time_info = get_time_info(body.video_name, video_in_process[body.video_name])
        done_videos_time[body.video_name] = video_in_process[body.video_name]
        video_in_process[body.video_name] = {}
        publisher.final_processing.publish(video_name=body.video_name)
        with open(time_status_path, 'w') as f:
            json.dump(done_videos_time, f, indent=2)

    if start_time - time() < config.GLOBAL.STATUS_TIME_INTERVAL:
        start_time = time()
        with open(global_status_path, 'w') as f:
            json.dump({k: v for k, v in video_in_process.items() if len(v) > 0}, f, indent=2)
    return True


def create_file_paths():
    st = localtime()
    global_status_file_name = 'status__%02d.%02d.%04d_%02d.%02d.txt' % \
                              (st.tm_mday, st.tm_mon, st.tm_year, st.tm_hour, st.tm_min)
    time_status_file_name = 'time_status__%02d.%02d.%04d_%02d.%02d.txt' % \
                              (st.tm_mday, st.tm_mon, st.tm_year, st.tm_hour, st.tm_min)
    return map(lambda p: os.path.join(config.GLOBAL.TMP_DIR, p), (global_status_file_name, time_status_file_name))


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str)
    return parser.parse_args()


if __name__ == '__main__':
    channel.queue_declare(queue=QUEUES.timer)
    channel.basic_qos(prefetch_count=1)
    channel.queue_declare(queue=STATUSES.timer)

    publisher = GlobalPublisher(channel)

    arguments = parse()
    with open(arguments.config, 'r') as f:
        config = edict(yaml.load(f))
    verbose_level = config.GLOBAL.VERBOSE

    steps_list = [
        'start_time',
        'image_parser',
        'body_tracker',
        'face_detector',
        'scene_change_detector',
        'face_tracker',
        'eye_tracker',
        'voice_separator'
    ]
    steps_set = set(steps_list)

    video_in_process = {}
    done_videos_time = {}
    global_status_path, time_status_path = create_file_paths()

    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(timer_callback, queue=QUEUES.timer, no_ack=True)
    channel.basic_publish(exchange='', routing_key=STATUSES.timer, body=json.dumps({'status': 'ready'}))

    channel.start_consuming()
