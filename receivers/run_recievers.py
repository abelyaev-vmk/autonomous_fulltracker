"""
NeurodataLab LLC 23.11.2017
Created by Andrey Belyaev

Declare all necessary queues
"""

import pika
from rabbitmq_config import *


if __name__ == '__main__':
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', heartbeat=0))
    channel = connection.channel()

    for queue_name, queue in QUEUES.items():
        channel.queue_declare(queue)
        channel.basic_qos(prefetch_count=1)

    for queue_name, queue in STATUSES.items():
        channel.queue_declare(queue)
        channel.basic_qos(prefetch_count=1)
