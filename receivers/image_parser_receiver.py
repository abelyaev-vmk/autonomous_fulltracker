"""
NeurodataLab LLC 20.11.2017
Created by Andrey Belyaev

Asynchronous image parser block
"""
import argparse
import logging
import os
import os.path as osp
import pika
import yaml
from rabbitmq_config import *
from shutil import rmtree
from subprocess import call
from time import time, sleep
from tracker_utils.error_handler import handle_receiver_errors

logger = logging.getLogger()
logger.setLevel(logging.INFO)


if __name__ == '__main__':
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', heartbeat=0))
    channel = connection.channel()
else:
    channel = None


@handle_receiver_errors(channel=channel)
def image_parser_callback(ch, method, properties, body):
    body = json.loads(body)
    start = time()
    video_path = body['video_path']
    video_name = '.'.join(video_path.strip().split('/')[-1].split('.')[:-1])
    video_info = videos_info[video_name]

    log_time(channel, start, 'start_time', video_name)

    video_temp_path = osp.join(config.GLOBAL.TMP_DIR, video_name)
    parsed_images_path = osp.join(video_temp_path, 'images')
    if osp.exists(video_temp_path):
        rmtree(video_temp_path)
    os.mkdir(video_temp_path)
    os.mkdir(parsed_images_path)

    dev_null = open('/dev/null', 'w')

    # parse images
    ffmpeg_params = ['ffmpeg', '-i', video_path, '-vsync', 'vfr', '-q:v', '2', '-filter:v',
                     'scale=%d:%d,%s,setpts=PTS-STARTPTS' %
                     (config.GLOBAL.WIDTH, config.GLOBAL.HEIGHT, fragments_info(video_info, video_temp_path)),
                     osp.join(parsed_images_path, 'image_%08d.jpg')]
    # print reduce(lambda a, b: a + ' ' + b, ffmpeg_params)
    ret = call(ffmpeg_params, stdout=dev_null, stderr=dev_null)

    # get wav
    audio_path = osp.join(video_temp_path, '%s.wav' % video_name)
    ffmpeg_params = ['ffmpeg', '-i', video_path, audio_path]
    ret = call(ffmpeg_params, stdout=dev_null, stderr=dev_null)

    connection.process_data_events()

    log_debug(channel, 'Image parser on %s done (ret=%d) %d images' %
              (video_name, ret, len(os.listdir(parsed_images_path))))

    publisher.body_tracker.publish(video_name=video_name, data_path=parsed_images_path,
                                   human_position=video_info['side'])
    publisher.face_detector.publish(video_name=video_name, data_path=parsed_images_path)
    publisher.voice_separator.publish(video_name=video_name, audio_path=audio_path)
    log_time(channel, time() - start, 'image_parser', video_name)

    return True


def fragments_info(video_info, tmp_path):
    fragments = video_info['fragments']
    if video_info['fragments_type'] == 'sec':
        fragments = list(map(lambda frag: map(lambda f: int(f * video_info['fps']), frag), fragments))
    scenes = video_info['scenes']
    if video_info['scenes_type'] == 'sec':
        scenes = list(map(lambda scene: map(lambda sc: int(sc * video_info['fps']), scene), scenes))

    cur_scene_num = 0
    cur_scene = scenes[cur_scene_num]
    new_fragments = []
    for frag in fragments:
        while not cur_scene[0] <= frag[0] <= cur_scene[1]:
            cur_scene_num += 1
            cur_scene = scenes[cur_scene_num] if cur_scene_num < len(scenes) else None
        if cur_scene is None:
            break
        new_fragments.append([max(cur_scene[0] + config.IMAGEPARSER.SCENE_LEFT_BORDER_MIN_OFFSET_FRAMES,
                                  frag[0] - config.IMAGEPARSER.FRAG_LEFT_BORDER_OFFSET_SEC), frag[1]])

    all_fragments = []
    for frag in new_fragments:
        all_fragments += list(range(frag[0], frag[1] + 1))
    with open(osp.join(tmp_path, 'fragments_info.json'), 'w') as f:
        json.dump([video_info['fps'], all_fragments], f)

    if len(new_fragments) == 0:
        return []
    else:
        ffmpeg_str = ''
        for frag in new_fragments:
            ffmpeg_str += 'between(n\,%d\,%d)+' % (frag[0], frag[1])
        ffmpeg_str = "select='%s'" % ffmpeg_str[:-1]
        return ffmpeg_str


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str)
    return parser.parse_args()


if __name__ == '__main__':
    channel.queue_declare(queue=QUEUES.image_parser)
    channel.basic_qos(prefetch_count=1)
    channel.queue_declare(queue=STATUSES.image_parser)

    publisher = GlobalPublisher(channel)

    arguments = parse()
    with open(arguments.config, 'r') as f:
        config = edict(yaml.load(f))
    with open(config.GLOBAL.VIDEOS_INFO, 'r') as f:
        videos_info = json.load(f)
    videos_info = {vi['video_name']: vi for vi in videos_info}

    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(image_parser_callback, queue=QUEUES.image_parser, no_ack=True)
    channel.basic_publish(exchange='', routing_key=STATUSES.image_parser, body=json.dumps({'status': 'ready'}))
    channel.start_consuming()
