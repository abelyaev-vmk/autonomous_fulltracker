"""
NeurodataLab LLC 20.11.2017
Created by Andrey Belyaev

Asynchronous eye tracker block
"""
import argparse
import os.path as osp
import pika
import yaml
from rabbitmq_config import *
from time import time
from EyeTracker.model import EyeTrackerModel
from tracker_utils.error_handler import handle_receiver_errors


if __name__ == '__main__':
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', heartbeat=0))
    channel = connection.channel()
else:
    channel = None


@handle_receiver_errors(channel=channel)
def eye_tracker_callback(ch, method, properties, body):
    body = edict(json.loads(body))
    start = time()
    for id in range(body.persons_count):
        out_name = osp.join(config.GLOBAL.TMP_DIR, body.video_name, body.video_name + '_eyes_id%d.json' % id)
        model.predict(body.data_path, body.persons_paths[id], out_name)
    connection.process_data_events()
    log_debug(channel, 'eye tracker on %s done' % body.video_name)
    log_time(channel, time() - start, 'eye_tracker', body.video_name)


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str)
    return parser.parse_args()


if __name__ == '__main__':

    channel.queue_declare(queue=QUEUES.eye_tracker)
    channel.queue_declare(queue=STATUSES.eye_tracker)

    publisher = GlobalPublisher(channel)

    arguments = parse()
    with open(arguments.config, 'r') as f:
        config = edict(yaml.load(f))
    et_config = config.EYETRACKER

    model = EyeTrackerModel(et_config.CLASSIFIER, et_config.REGRESSOR)

    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(eye_tracker_callback, queue=QUEUES.eye_tracker, no_ack=True)
    channel.basic_publish(exchange='', routing_key=STATUSES.eye_tracker, body=json.dumps({'status': 'ready'}))

    channel.start_consuming()
