"""
NeurodataLab LLC 20.11.2017
Created by Andrey Belyaev

Status receiver waits for all asynchronous blocks to initialize
"""
import json
import pika
from rabbitmq_config import *
from tracker_utils.error_handler import handle_receiver_errors


if __name__ == '__main__':
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', heartbeat=0))
    channel = connection.channel()
else:
    channel = None


@handle_receiver_errors(channel=channel)
def queue_callback(queue_name):
    def callback(ch, method, properties, body):
        body = json.loads(body)
        print '%s now has status = %s' % (queue_name, body['status'])
        current_statuses[queue_name] = body['status']
        not_ready_components = list(filter(lambda q: current_statuses[q] != 'ready', current_statuses))
        if len(not_ready_components) == 0:
            print 'All receivers ready'
            exit()
        return True

    return callback

if __name__ == '__main__':
    current_statuses = {key: 'not ready' for key in STATUSES.keys()}

    for queue_name, queue in STATUSES.items():
        try:
            channel.basic_consume(queue_callback(queue_name), queue=queue, no_ack=True)
        except:
            print 'Channel off:', queue_name

    channel.start_consuming()
