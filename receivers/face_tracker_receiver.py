"""
NeurodataLab LLC 20.11.2017
Created by Andrey Belyaev

Asynchronous face tracker block
"""
import argparse
import os.path as osp
import pika
import yaml
from rabbitmq_config import *
from time import time
from FaceTracker.model import FaceTrackerModel
from tracker_utils.error_handler import handle_receiver_errors


if __name__ == '__main__':
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', heartbeat=0))
    channel = connection.channel()
else:
    channel = None


@handle_receiver_errors(channel=channel)
def face_tracker_callback(ch, method, properties, body):
    body = edict(json.loads(body))
    start = time()
    persons_paths = model.cluster_video(body.data_path, body.face_marking, body.scenes_changes,
                                        osp.join(config.GLOBAL.TMP_DIR, body.video_name), body.video_name,
                                        videos_info[body.video_name]['side'])
    connection.process_data_events()
    publisher.eye_tracker.publish(data_path=body.data_path, video_name=body.video_name,
                                  persons_count=len(persons_paths), persons_paths=persons_paths)
    log_debug(channel, 'face tracker on %s done' % body.video_name)
    log_time(channel, time() - start, 'face_tracker', body.video_name)
    return True


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str)
    return parser.parse_args()


if __name__ == '__main__':
    channel.queue_declare(queue=QUEUES.face_tracker)
    channel.queue_declare(queue=STATUSES.face_tracker)

    publisher = GlobalPublisher(channel)

    arguments = parse()
    with open(arguments.config, 'r') as f:
        config = edict(yaml.load(f))
    with open(config.GLOBAL.VIDEOS_INFO, 'r') as f:
        videos_info = json.load(f)
    videos_info = {vi['video_name']: vi for vi in videos_info}

    ft_config = config.FACETRACKER
    model = FaceTrackerModel(ft_config.NET,
                             ft_config.FACE_WIDTH, ft_config.FACE_HEIGHT,
                             ft_config.GPU,
                             ft_config.FEAT_LAYER, ft_config.MAX_FEAT_DIFF, ft_config.MAX_MISSED_FRAMES_COUNT,
                             ft_config.BATCH_SIZE)

    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(face_tracker_callback, queue=QUEUES.face_tracker, no_ack=True)
    channel.basic_publish(exchange='', routing_key=STATUSES.face_tracker, body=json.dumps({'status': 'ready'}))

    channel.start_consuming()
