"""
NeurodataLab LLC 20.11.2017
Created by Andrey Belyaev

Asynchronous face detector block
"""
import argparse
import os.path as osp
import pika
import yaml
from rabbitmq_config import *
from time import time
from tracker_utils.error_handler import handle_receiver_errors
from FaceDetector.model import init_model


if __name__ == '__main__':
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', heartbeat=0))
    channel = connection.channel()
else:
    channel = None


@handle_receiver_errors(channel=channel)
def face_detector_callback(ch, method, properties, body):
    body = edict(json.loads(body.decode('utf-8')))
    start = time()
    # all_faces_marking_path = osp.join(config.GLOBAL.TMP_DIR, body.video_name, body.video_name + '_faces.json')
    #
    # if check_previous(all_faces_marking_path):
    #     new_marking_path = get_clean_marking_path(all_faces_marking_path)
    # else:

    face_marking = process_video(body.video_name, body.data_path)
    new_marking_path = parse_marking(body.video_name, face_marking)

    publisher.scene_change_detector.publish(video_name=body.video_name,
                                            data_path=body.data_path,
                                            face_marking=face_marking)

    # publisher.face_tracker.publish(video_name=body.video_name, data_path=body.data_path,
    #                                face_marking=face_marking, scenes_changes=json.dumps({}))
    log_debug(channel, 'face detector on %s done' % body.video_name)
    log_time(channel, time() - start, 'face_detector', body.video_name)
    return True


def check_previous(marking_path):
    return osp.exists('%s_cleaned.json' % '.'.join(marking_path.split('.')[:-1]))


def get_clean_marking_path(marking_path):
    return '%s_cleaned.json' % '.'.join(marking_path.split('.')[:-1])


def parse_marking(video_name, marking_path):
    with open(marking_path, 'r') as f:
        marking = json.load(f)
    side = videos_info[video_name]['side']
    new_marking = {}
    for key, val in marking.items():
        if len(val) in (0, 1):
            new_marking[key] = val
        if len(val) >= 2:
            if side == 'left':
                new_marking[key] = [sorted(val, key=lambda v: v['x'])[0]]
            elif side == 'right':
                new_marking[key] = [sorted(val, key=lambda v: v['x'])[-1]]
            else:
                new_marking[key] = []

    new_marking_path = get_clean_marking_path(marking_path)
    with open(new_marking_path, 'w') as f:
        json.dump(new_marking, f, indent=2)
    return new_marking_path


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str)
    return parser.parse_args()


if __name__ == '__main__':

    channel.queue_declare(queue=QUEUES.face_detector)
    channel.basic_qos(prefetch_count=1)
    channel.queue_declare(queue=STATUSES.face_detector)

    publisher = GlobalPublisher(channel)

    arguments = parse()
    with open(arguments.config, 'r') as f:
        config = edict(yaml.load(f))
    with open(config.GLOBAL.VIDEOS_INFO, 'r') as f:
        videos_info = json.load(f)
    videos_info = {vi['video_name']: vi for vi in videos_info}

    process_video = init_model(config)

    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(face_detector_callback, queue=QUEUES.face_detector, no_ack=True)
    channel.basic_publish(exchange='', routing_key=STATUSES.face_detector, body=json.dumps({'status': 'ready'}))

    channel.start_consuming()
