"""
NeurodataLab LLC 22.01.2018
Created by Andrey Belyaev

Asynchronous voice separator block
"""

import argparse
import os.path as osp
import pika
import yaml
from rabbitmq_config import *
from VoiceSeparator.model import VoiceSeparator
from time import time
from tracker_utils.error_handler import handle_receiver_errors

if __name__ == '__main__':
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', heartbeat=0))
    channel = connection.channel()
else:
    channel = None


@handle_receiver_errors(channel=channel)
def voice_separator_callback(ch, method, properties, body):
    body = edict(json.loads(body.decode('utf-8')))
    start = time()
    video_info = videos_info[body.video_name]
    if video_info['fragments_type'] == 'sec':
        fragments_pairs = video_info['fragments']
    else:
        fragments_pairs = map(lambda pair: map(lambda p: p / float(video_info['fps']), pair), video_info['fragments'])
    model.process(body.audio_path, fragments_pairs, osp.join(config.GLOBAL.TMP_DIR, body.video_name), [1, 2, 3])
    log_debug(channel, 'voice separator on %s done' % body.video_name)
    log_time(channel, time() - start, 'voice_separator', body.video_name)
    return True


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str)
    return parser.parse_args()


if __name__ == '__main__':
    channel.queue_declare(queue=QUEUES.voice_separator)
    channel.basic_qos(prefetch_count=1)
    channel.queue_declare(queue=STATUSES.voice_separator)

    arguments = parse()

    with open(arguments.config, 'r') as f:
        config = edict(yaml.load(f))
    with open(config.GLOBAL.VIDEOS_INFO, 'r') as f:
        videos_info = json.load(f)
    videos_info = {vi['video_name']: vi for vi in videos_info}

    vs_config = config.VOICESEPARATOR
    model = VoiceSeparator(**dict(vs_config))

    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(voice_separator_callback, queue=QUEUES.voice_separator, no_ack=True)
    channel.basic_publish(exchange='', routing_key=STATUSES.voice_separator, body=json.dumps({'status': 'ready'}))

    channel.start_consuming()
