"""
NeurodataLab LLC 20.11.2017
Created by Andrey Belyaev

Asynchronous logs block
"""
import argparse
import json
import pika
import time
import yaml
from rabbitmq_config import *
from tracker_utils.error_handler import handle_receiver_errors


if __name__ == '__main__':
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', heartbeat=0))
    channel = connection.channel()
else:
    channel = None


@handle_receiver_errors(channel=channel)
def logs_callback(ch, method, properties, body):
    body = json.loads(body)
    msg_level = body['msg_level']

    st = time.localtime()
    if msg_level == 3:
        msg_type = 'DEBUG'
    else:
        msg_type = 'WARN' if msg_level == 1 else 'INFO'
    msg = ('[%s %02d:%02d:%04d %02d:%02d:%02d] '
           % (msg_type, st.tm_mday, st.tm_mon, st.tm_year, st.tm_hour, st.tm_min, st.tm_sec)) + body['msg']
    log_file.write(msg + '\n')
    if body['msg_level'] <= verbose_level:
        print msg

    connection.process_data_events()

    return True


def create_log_file():
    st = time.localtime()
    log_file_name = 'logs__%02d.%02d.%04d_%02d.%02d.txt' % (st.tm_mday, st.tm_mon, st.tm_year, st.tm_hour, st.tm_min)
    return open(os.path.join(config.GLOBAL.TMP_DIR, log_file_name), 'w', buffering=0)


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str)
    return parser.parse_args()


if __name__ == '__main__':
    channel.queue_declare(queue=QUEUES.logger)
    channel.basic_qos(prefetch_count=1)
    channel.queue_declare(queue=STATUSES.logger)

    arguments = parse()
    with open(arguments.config, 'r') as f:
        config = edict(yaml.load(f))
    verbose_level = config.GLOBAL.VERBOSE

    log_file = create_log_file()

    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(logs_callback, queue=QUEUES.logger, no_ack=True)
    channel.basic_publish(exchange='', routing_key=STATUSES.logger, body=json.dumps({'status': 'ready'}))

    channel.start_consuming()
