"""
NeurodataLab LLC 20.11.2017
Created by Andrey Belyaev

Channels config
"""
import json
import os
import sys
import warnings
from easydict import EasyDict as edict

sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
warnings.filterwarnings("ignore", category=DeprecationWarning)
warnings.filterwarnings("ignore", category=FutureWarning)
warnings.filterwarnings("ignore", category=UserWarning)

QUEUES = edict(image_parser='image_parser',
               body_tracker='body_tracker',
               face_detector='face_detector',
               face_tracker='face_tracker',
               eye_tracker='eye_tracker',
               scene_change_detector='scene_change_detector',
               voice_separator='voice_separator',
               logger='logger',
               timer='timer',
               final_processing='final_processing')

STATUSES = edict(image_parser='image_parser_status',
                 body_tracker='body_tracker_status',
                 face_detector='face_detector_status',
                 face_tracker='face_tracker_status',
                 eye_tracker='eye_tracker_status',
                 scene_change_detector='scene_change_detector_status',
                 voice_separator='voice_separator_status',
                 logger='logger_status',
                 timer='timer_status',
                 final_processing='final_processing_status')


def log_info(channel, msg):
    # print (msg)
    body = {
        'msg_level': 2,
        'msg': msg
    }
    channel.basic_publish(exchange='', routing_key=QUEUES.logger, body=json.dumps(body))


def log_warn(channel, msg):
    # print (msg)
    body = {
        'msg_level': 1,
        'msg': msg
    }
    channel.basic_publish(exchange='', routing_key=QUEUES.logger, body=json.dumps(body))


def log_debug(channel, msg):
    # print (msg)
    body = {
        'msg_level': 3,
        'msg': msg
    }
    channel.basic_publish(exchange='', routing_key=QUEUES.logger, body=json.dumps(body))


def log_time(channel, time, receiver_name, video_name):
    # pass
    body = {
        'time': time,
        'receiver_name': receiver_name,
        'video_name': video_name
    }
    channel.basic_publish(exchange='', routing_key=QUEUES.timer, body=json.dumps(body))


class Publisher:
    def __init__(self, channel, key):
        self.channel, self.key = channel, key

    def publish(self, **kwargs):
        self.channel.basic_publish(exchange='', routing_key=self.key,
                                   body=json.dumps({k: v for k, v in kwargs.items()}))


class GlobalPublisher:
    def __init__(self, channel):
        self.channel = channel
        self.image_parser = Publisher(channel, QUEUES.image_parser)
        self.body_tracker = Publisher(channel, QUEUES.body_tracker)
        self.face_detector = Publisher(channel, QUEUES.face_detector)
        self.face_tracker = Publisher(channel, QUEUES.face_tracker)
        self.eye_tracker = Publisher(channel, QUEUES.eye_tracker)
        self.scene_change_detector = Publisher(channel, QUEUES.scene_change_detector)
        self.voice_separator = Publisher(channel, QUEUES.voice_separator)
        self.timer = Publisher(channel, QUEUES.timer)
        self.final_processing = Publisher(channel, QUEUES.final_processing)
