"""
NeurodataLab LLC 11.01.2018
Created by Andrey Belyaev
"""

import argparse
import os.path as osp
import pika
import pika
import yaml
from rabbitmq_config import *
from time import time
from SceneChangeDetector.model import SceneChangeDetectorModel
from tracker_utils.error_handler import handle_receiver_errors


if __name__ == '__main__':
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', heartbeat=0))
    channel = connection.channel()
else:
    channel = None


@handle_receiver_errors(channel=channel)
def scene_change_detector_callback(ch, method, properties, body):
    body = edict(json.loads(body))
    start = time()

    scenes_changes = model.process_video(body.data_path)

    connection.process_data_events()
    publisher.face_tracker.publish(video_name=body.video_name, data_path=body.data_path,
                                   face_marking=body.face_marking, scenes_changes=scenes_changes)
    log_debug(channel, 'Scene change detector on %s done' % body.video_name)
    log_time(channel, time() - start, 'scene_change_detector', body.video_name)
    return True


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str)
    return parser.parse_args()


if __name__ == '__main__':
    channel.queue_declare(queue=QUEUES.scene_change_detector)
    channel.basic_qos(prefetch_count=1)
    channel.queue_declare(queue=STATUSES.scene_change_detector)

    publisher = GlobalPublisher(channel)

    arguments = parse()
    with open(arguments.config, 'r') as f:
        config = edict(yaml.load(f))

    scd_config = config.SCENECHANGEDETECTOR
    model = SceneChangeDetectorModel(scd_config.NET,
                                     scd_config.WIDTH, scd_config.HEIGHT,
                                     scd_config.GPU,
                                     scd_config.CHANGE_THRESHOLD,
                                     scd_config.DATA_NAMES, scd_config.LABEL_NAMES,
                                     scd_config.BATCH_SIZE)

    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(scene_change_detector_callback, queue=QUEUES.scene_change_detector, no_ack=True)
    channel.basic_publish(exchange='', routing_key=STATUSES.scene_change_detector, body=json.dumps({'status': 'ready'}))

    channel.start_consuming()
