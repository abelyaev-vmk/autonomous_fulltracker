"""
NeurodataLab LLC 24.01.2018
Created by Andrey Belyaev
"""

import argparse
import numpy as np
import os
import os.path as osp
import pika
import pickle
import yaml
from rabbitmq_config import *
from shutil import copyfile, rmtree
from subprocess import call
from tracker_utils.error_handler import handle_receiver_errors
from VideoAnalyzer.video_analyzer import VideoAnalyzer

dev_null = open('/dev/null', 'w')


def create_key_transformer(fragments, fps):
    def key_transformer(key):
        num = int(key.split('_')[-1].split('.')[0])
        return fragments[num - 1], fragments[num - 1] / fps

    return key_transformer


if __name__ == '__main__':
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', heartbeat=0))
    channel = connection.channel()
else:
    channel = None


# @handle_receiver_errors(channel=channel)
def final_processing_callback(ch, method, properties, body):
    body = edict(json.loads(body))

    tmp_video_path = osp.join(config.GLOBAL.TMP_DIR, body.video_name)
    out_video_path = osp.join(config.GLOBAL.OUT_DIR, body.video_name)
    if not osp.exists(out_video_path):
        os.mkdir(out_video_path)

    video_files = os.listdir(tmp_video_path)

    with open(osp.join(tmp_video_path, 'fragments_info.json'), 'r') as f:
        fps, fragments = json.load(f)
    transform_key = create_key_transformer(fragments, fps)

    all_faces_marking_path = '%s_faces.json' % body.video_name
    cleaned_faces_marking_path = '%s_faces_cleaned.json' % body.video_name

    body_keypoints_path = '%s_body-keypoints.npy' % body.video_name
    body_keypoints = np.load(osp.join(tmp_video_path, body_keypoints_path)).tolist()

    faces_id_paths = sorted(filter(lambda vf: 'faces_id' in vf, video_files))
    faces_id_markings = sorted(filter(lambda fip: fip.endswith('json'), faces_id_paths))
    faces_id_npy = sorted(filter(lambda fip: fip.endswith('npy'), faces_id_paths))
    eye_markings = sorted(filter(lambda vf: 'eyes' in vf, video_files))

    output_marking = {}
    key_corresponding = {}
    for n, (face_id_marking_path, face_id_npy_path, eye_id_marking_path) in \
            enumerate(zip(faces_id_markings, faces_id_npy, eye_markings)):

        with open(osp.join(tmp_video_path, face_id_marking_path), 'r') as f:
            id_marking = json.load(f)
        with open(osp.join(tmp_video_path, eye_id_marking_path), 'r') as f:
            eye_marking = json.load(f)
        id_npy = np.load(osp.join(tmp_video_path, face_id_npy_path)).tolist()

        for key, val in id_marking.items():
            frame, sec = transform_key(key)
            key_corresponding[frame] = key
            if frame not in output_marking:
                output_marking[frame] = {
                    'sec': sec,
                    'face': [(n, val)],
                    'feat': [(n, np.array(id_npy[key]).tolist()) if key in id_npy.keys() else None],
                    'eyes': [(n, eye_marking[key]) if key in eye_marking else None],
                    'body': body_keypoints[key] if key in body_keypoints else None
                }
            else:
                output_marking[frame]['face'].append((n, val))
                output_marking[frame]['eyes'].append((n, eye_marking[key]) if key in eye_marking else None)
                output_marking[frame]['feat'].append((n, np.array(id_npy[key]).tolist())
                                                     if key in id_npy.keys() else None)

    with open(osp.join(out_video_path, 'marking.pckl'), 'w') as f_:
        pickle.dump(output_marking, f_)
    with open(osp.join(out_video_path, 'key_corresponding.json'), 'w') as f:
        json.dump(key_corresponding, f, indent=2)
    copyfile(osp.join(tmp_video_path, all_faces_marking_path), osp.join(out_video_path, 'all_faces.json'))
    copyfile(osp.join(tmp_video_path, cleaned_faces_marking_path), osp.join(out_video_path, 'cleaned_faces.json'))

    log_info(channel, 'Analyzing video %s' % body.video_name)
    video_analyzer = VideoAnalyzer.process_video(osp.join(out_video_path, 'marking.pckl'), body.video_name,
                                                 videos_info[body.video_name]['side'])

    log_info(channel, 'Serializing video analyzer on %s' % body.video_name)
    video_analyzer.serialize(out_video_path, to_pickle=True, to_json=True)

    if arguments.visualize:
        log_info(channel, 'Visualizing %s' % body.video_name)
        video_analyzer.visualize(osp.join(tmp_video_path, 'images'),
                                 osp.join(out_video_path, 'visualize'),
                                 osp.join(out_video_path, 'key_corresponding.json'))

    if arguments.copy_voices:
        log_info(channel, 'Copying voices in %s' % body.video_name)
        voices_path = osp.join(tmp_video_path, 'voices')
        if osp.exists(voices_path):
            ret = call(['cp', '-r', voices_path, osp.join(out_video_path, 'voices')], stderr=dev_null, stdout=dev_null)
            if ret != 0:
                log_info(channel, 'Copying voices exit with status %d on %s' % (ret, body.video_name))

    if arguments.copy_images:
        log_info(channel, 'Copying images in %s' % body.video_name)
        images_path = osp.join(tmp_video_path, 'images')
        if osp.exists(images_path):
            ret = call(['cp', '-r', images_path, osp.join(out_video_path, 'images')], stderr=dev_null, stdout=dev_null)
            if ret != 0:
                log_info(channel, 'Copying images exit with status %d on %s' % (ret, body.video_name))

    if arguments.delete_temp_files:
        rmtree(tmp_video_path)

    log_info(channel, 'All files processed on %s' % body.video_name)

    return True


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str)
    parser.add_argument('--visualize', action='store_true')
    parser.add_argument('--copy-voices', action='store_true')
    parser.add_argument('--copy-images', action='store_true')
    parser.add_argument('--delete-temp-files', action='store_true')
    return parser.parse_args()


if __name__ == '__main__':
    channel.queue_declare(queue=QUEUES.final_processing)
    channel.basic_qos(prefetch_count=1)
    channel.queue_declare(queue=STATUSES.final_processing)

    arguments = parse()
    with open(arguments.config, 'r') as f:
        config = edict(yaml.load(f))
    with open(config.GLOBAL.VIDEOS_INFO, 'r') as f:
        videos_info = json.load(f)
    videos_info = {vi['video_name']: vi for vi in videos_info}

    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(final_processing_callback, queue=QUEUES.final_processing, no_ack=True)
    channel.basic_publish(exchange='', routing_key=STATUSES.final_processing, body=json.dumps({'status': 'ready'}))

    channel.start_consuming()
