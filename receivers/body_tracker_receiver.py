"""
NeurodataLab LLC 20.11.2017
Created by Andrey Belyaev

Asynchronous body tracker block
"""
import argparse
import pika
import os.path as osp
import yaml
from rabbitmq_config import *
from time import time
from BodyTracker.model import BodyTrackerModel
from tracker_utils.error_handler import handle_receiver_errors


if __name__ == '__main__':
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', heartbeat=0))
    channel = connection.channel()
else:
    channel = None


@handle_receiver_errors(channel=channel)
def body_tracker_callback(ch, method, properties, body):
    body = edict(json.loads(body))
    start = time()
    out_file = osp.join(config.GLOBAL.TMP_DIR, body.video_name, body.video_name + '_body-keypoints.npy')
    model.predict_on_folder(body.data_path, out_file, body.human_position)
    connection.process_data_events()
    log_debug(channel, 'body tracker on %s done' % body.video_name)
    log_time(channel, time() - start, 'body_tracker', body.video_name)
    return True


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config', type=str)
    return parser.parse_args()


if __name__ == '__main__':

    channel.queue_declare(queue=QUEUES.body_tracker)
    channel.basic_qos(prefetch_count=1)
    channel.queue_declare(queue=STATUSES.body_tracker)

    arguments = parse()
    with open(arguments.config, 'r') as f:
        config = edict(yaml.load(f))

    bt_config = config.BODYTRACKER
    model = BodyTrackerModel(bt_config.NET, bt_config.WIDTH, bt_config.HEIGHT, bt_config.GPU, bt_config.BATCH_SIZE)

    channel.basic_consume(body_tracker_callback, queue=QUEUES.body_tracker, no_ack=True)
    channel.basic_publish(exchange='', routing_key=STATUSES.body_tracker, body=json.dumps({'status': 'ready'}))

    channel.start_consuming()
